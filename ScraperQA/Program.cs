﻿using Newtonsoft.Json;
using ScraperQA.ArpaeScraper.ArpaeWeb;
using ScraperQA.ArpaeScraper.DailyClimateAnalysis;
using ScraperQA.ArpaeScraper.RFC_rmap;
using ScraperQA.Database;
using ScraperQA.Database.Conditions;
using ScraperQA.Database.Creation;
using ScraperQA.Database.Queries;
using ScraperQA.Database.WebAPI;
using ScraperQA.OpenAQ;
using ScraperQA.OpenWeatherMap;
using ScraperQA.Utility;
using ScraperQA.Utility.Files;
using ScraperQA.Utility.GeoUtility;
using ScraperQA.Utility.Physics;
using ScraperQA.Utility.Visualization;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace ScraperQA
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleManager.Start(args);
            ///TODO: Move all main method into ConsoleManager.cs
            return;

            Settings settings = JsonConvert.DeserializeObject<Settings>(File.ReadAllText("settings.json"));


            DbCreator creator = new DbCreator(settings.Db.Host, settings.Db.Username, settings.Db.Password, settings.Db.Database);
            creator.Execute();

            DatabaseManager db = new DatabaseManager(String.Format("Host={0};Username={1};Password={2};Database={3};",
                settings.Db.Host, settings.Db.Username, settings.Db.Password, settings.Db.Database));

            Console.Write("Insert arpae air quality values? [Y/n] ");
            if (Console.ReadLine().ToLower() == "y")
            {
                InsertArpaeAirQualityRecords(db);
            }

            Console.Write("Insert arpae daily values? [Y/n] ");
            if (Console.ReadLine().ToLower() == "y")
            {
                InsertArpaeDailyRecords(db);
            }

            Console.Write("Insert arpae JSON weather values? [Y/n] ");
            if (Console.ReadLine().ToLower() == "y")
            {
                Console.Write("Bulk insert? [Y/n] ");
                InsertArpaeJSONWeather(db, settings.Rmap.JSONPath, Console.ReadLine().ToLower() == "y");
            }

            Console.Write("Insert arpae CSV air quality values? [Y/n] ");
            if (Console.ReadLine().ToLower() == "y")
            {
                Console.Write("Bulk insert? [Y/n] ");
                InsertArpaeCSV_AQ(db, settings.Aqcsv.CSVPath, Console.ReadLine().ToLower() == "y");
            }

            OwmUpdater updater = null;
            Console.Write("Insert owm ER weather current values? [Y/n] ");
            if (Console.ReadLine().ToLower() == "y")
            {
                updater = new OwmUpdater(new OwmClient(settings.Owm), db, 40);
                Console.Write("Center latitude: ");
                double lat = double.Parse(Console.ReadLine());
                Console.Write("Center longitude: ");
                double lon = double.Parse(Console.ReadLine());
                Console.Write("Radius (km): ");
                double km = double.Parse(Console.ReadLine());
                updater.Cities = OwmHelper.GetCityInCircleArea(OwmHelper.GetAllCityFromFile(), lat, lon, km).ToList();
                updater.Start(OwmUpdater.UpdateType.City);
            }

            Console.Write("Insert arpae Web stations? [Y/n] ");
            if (Console.ReadLine().ToLower() == "y")
            {
                db.Insert(ArpaeScraper.ArpaeWeb.Station.GetAllDbStations());

            }

            Console.Write("Insert arpae JSON stations? [Y/n] ");
            if (Console.ReadLine().ToLower() == "y")
            {
                db.Insert(JSONParser.dbStations(Directory.EnumerateFiles(settings.Rmap.JSONPath, "*.json")));
            }

            Console.Write("Insert arpae CSV aq stations? [Y/n] ");
            if (Console.ReadLine().ToLower() == "y")
            {
                db.Insert(ArpaeScraper.AQ_csv.Stations.GetDbStations());
            }

            Console.Write("Insert PurpleAir stations? [Y/n] ");
            if (Console.ReadLine().ToLower() == "y")
            {
                db.Insert(PurpleAirScraper.Stations.GetAllStations());
                db.Insert(
                PurpleAirScraper.Stations.GetAllStations().SelectMany(s => (new List<Database.ValueType>() { Database.ValueType.PM1, Database.ValueType.PM25, Database.ValueType.PM10, Database.ValueType.Temperature, Database.ValueType.Humidity }).Select(val_type => new Queries.StationParameterPair(s, val_type)))
                );
            }

            Console.Write("Insert value types? [Y/n] ");
            if (Console.ReadLine().ToLower() == "y")
            {
                db.Insert(Database.DbValueType.GetDbValueTypes());
            }

            Console.Write("Insert aggregation types? [Y/n] ");
            if (Console.ReadLine().ToLower() == "y")
            {
                db.Insert(Database.DbAggregationType.GetDbAggregationTypes());
            }

            Console.Write("Write stations to CSV? [Y/n] ");
            if (Console.ReadLine().ToLower() == "y")
            {
                Console.Write("Insert value type parameter: ");
                int vt = 0;
                IEnumerable<Queries.StationParameterPair> stationParameterPairs = null;
                if (int.TryParse(Console.ReadLine(), out vt))
                {
                    stationParameterPairs = db.Select<Queries.StationParameterPair>(
                    (new DbColumnName() { Name = "id_value_type" }) == vt);
                }
                else
                {
                    stationParameterPairs = db.Select<Queries.StationParameterPair>();
                }

                //IEnumerable<Queries.StationParameterPair> filtered = stationParameterPairs.Where(s => Utility.GeoUtility.GeoUtility.IsInsideCircle(0, 0, 0, s.Latitude, s.Longitude));
                FileUtils.WriteCSV<DbStation>("Stations.csv", ",", stationParameterPairs);
            }

            Console.ReadKey();
            if (updater != null)
            {
                updater.Stop();
            }
            Logger.Log("Scraper finished!");

        }

        static void InsertArpaeDailyRecords(DatabaseManager db)
        {
            Logger.Log("Star inserting arpae daily values...", LogType.Warning, true);
            Extractor ex = new Extractor("celle arpae.csv", "tmin.txt", "tmax.txt", "prec.txt");
            db.BulkInsert(ex.Read(null).Where(r => r.Date >= new DateTime(2000, 1, 1)));
            Logger.Log("Finish inserting arpae daily values", LogType.Warning, true);
        }

        static void InsertArpaeAirQualityRecords(DatabaseManager db)
        {
            List<Record> records = new List<Record>();
            List<Record> tmp = new List<Record>();
            DateTime end = DateTime.Now.AddMonths(2);

            foreach (Province pv in Enum.GetValues(typeof(Province)))
            {
                List<ArpaeScraper.ArpaeWeb.Station> stations = ArpaeScraper.ArpaeWeb.Station.GetStationByProvince(pv);
                foreach (ArpaeScraper.ArpaeWeb.Station s in stations)
                {
                    DateTime idate = new DateTime(2000, 1, 1);
                    while (idate < end)
                    {
                        tmp = s.GetRecords(idate, idate.AddMonths(1));
                        Logger.Log(String.Format("Loaded {0} records from station {1}-{2} from {3} to {4}", tmp.Count, s.Name, s.Id, idate, idate.AddMonths(1)), LogType.Warning);
                        db.Insert(tmp);
                        idate = idate.AddMonths(1);
                    }
                }
            }
        }

        static void InsertArpaeJSONWeather(DatabaseManager db, string folder, bool bulkinsert = false)
        {
            if (!bulkinsert)
            {
                Action<IEnumerable<Record>, int> act = (r, i) =>
                {
                    List<Record> failed = db.Insert(r);
                    //Logger.Log(String.Format("Inserted {0} out of {1} records.", r.Count() - failed.Count(), r.Count()));
                };

                act += FileUtils.GetOnNewRowDefault<Record>();

                FileUtils.IterateRows<Record>(Directory.EnumerateFiles(folder, "*.json"), ScraperQA.ArpaeScraper.RFC_rmap.Converters.StringToRecordListConverter, FileUtils.OnNewFileDefault, act);
            }
            else
            {
                foreach (string file in Directory.EnumerateFiles(folder, "*.json"))
                {
                    db.BulkInsertInBatches(FileUtils.IterateRows<Record>(new List<string> { file }, ScraperQA.ArpaeScraper.RFC_rmap.Converters.StringToRecordListConverter));
                    Logger.Log(String.Format("File {0} inserted!", file));
                }
            }
        }

        static void InsertArpaeCSV_AQ(DatabaseManager db, string folder, bool bulkinsert = false)
        {
            if (!bulkinsert)
            {
                Action<IEnumerable<Record>, int> act = (r, i) =>
            {
                List<Record> failed = db.Insert(r);
            };

                act += FileUtils.GetOnNewRowDefault<Record>();

                FileUtils.IterateRows<Record>(Directory.EnumerateFiles(folder, "*.csv"), ArpaeScraper.AQ_csv.Converters.ToRecordConverter, FileUtils.OnNewFileDefault, act);
            }
            else
            {
                foreach (string file in Directory.EnumerateFiles(folder, "*.csv"))
                {
                    db.BulkInsertInBatches(FileUtils.IterateRows<Record>(new List<string> { file }, ArpaeScraper.AQ_csv.Converters.ToRecordConverter));
                    Logger.Log(String.Format("File {0} inserted!", file));
                }
            }
        }

        static string GenerateMarkerData(DatabaseManager db, IEnumerable<Record> records)
        {
            var result = records.GroupBy(x => x.Source);
            var vType = db.Select<DbValueType>().ToDictionary(x => x.Id);
            var aType = db.Select<DbAggregationType>().ToDictionary(x => x.Id);
            var stations = db.Select<DbStation>().ToDictionary(x => x.Id);

            List<Marker> markers = new List<Marker>();
            foreach (var res in result)
            {
                Marker m = new Marker();
                if (stations.ContainsKey((int)res.Key))
                {
                    m.Title = stations[(int)res.Key].Name;
                    m.Position = new MarkerPosition() { Latitude = (double)stations[(int)res.Key].Latitude, Longitude = (double)stations[(int)res.Key].Longitude };
                }
                else
                {
                    m.Title = "[unknown]";
                    m.Position = null;
                }
                m.Label = res.Count().ToString();
                m.Values = new List<MarkerValue>();
                foreach (Record r in res)
                {
                    m.Values.Add(new MarkerValue()
                    {
                        Name = vType[r.ValueType].Name,
                        Type = aType[r.AggregationType].Name,
                        Start = r.Date.ToString("yyyy-MM-dd HH:mm:ss"),
                        End = r.Date.Add(r.Span).ToString("yyyy-MM-dd HH:mm:ss"),
                        Unit = vType[r.ValueType].Unit,
                        Value = r.Value,
                    });
                    if (m.Position == null)
                    {
                        m.Position = new MarkerPosition() { Latitude = r.Latitude, Longitude = r.Longitude };
                    }
                }
                markers.Add(m);
            }

            string data = JsonConvert.SerializeObject(markers, Formatting.Indented);
            return data;
        }



    }





}
