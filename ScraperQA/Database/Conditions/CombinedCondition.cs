﻿namespace ScraperQA.Database.Conditions
{
    public abstract class CombinedCondition : Condition
    {
        public Condition Left { get; set; }
        public Condition Right { get; set; }
    }
    public class AndCondition : CombinedCondition
    {
        public override bool IsValid(object obj)
        {
            return this.Left.IsValid(obj) && this.Right.IsValid(obj);
        }
        public override string ToString()
        {
            return "(" + Left + " AND " + Right + ")";
        }
    }
    public class OrCondition : CombinedCondition
    {
        public override bool IsValid(object obj)
        {
            return this.Left.IsValid(obj) || this.Right.IsValid(obj);
        }
        public override string ToString()
        {
            return "(" + Left + " OR " + Right + ")";
        }
    }

    public class NotCondition : Condition
    {
        public override bool IsValid(object obj)
        {
            return !this.Value.IsValid(obj);
        }
        public Condition Value { get; set; }
        public override string ToString()
        {
            return "(NOT (" + Value + "))";
        }
    }
}
