﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace ScraperQA.Database.Conditions
{
    public class QueryCondition : Condition
    {
        private delegate object GetValue(object istance);

        private Dictionary<Type, GetValue> getters = new Dictionary<Type, GetValue>();
        public DbColumnName Column { get; set; }
        public QueryConditionOperator Operator { get; set; }
        public object Value { get; set; }

        public override bool IsValid(object obj)
        {
            GetValue gv = null;
            if (getters.ContainsKey(obj.GetType()))
            {
                gv = getters[obj.GetType()];
            }
            else
            {
                FieldInfo infoF = obj.GetType().GetFields().Where(it => it.Name == this.Column.Name).FirstOrDefault();
                
                if (infoF != null)
                {
                    gv = infoF.GetValue;
                }
                else
                {
                    PropertyInfo infoP = obj.GetType().GetProperties().Where(it => it.Name == this.Column.Name).FirstOrDefault();
                    if (infoP != null)
                    {
                        gv = infoP.GetValue;
                    }
                }

                if(gv != null)
                {
                    getters.Add(obj.GetType(), gv);
                }
            }

            
            if (gv == null)
            {
                return false;
            }

            object value = gv(obj);

            if (value is IComparable)
            {
                IComparable comp = value as IComparable;

                int res = comp.CompareTo(this.Value);
                switch (this.Operator)
                {
                    case QueryConditionOperator.E: return res == 0;
                    case QueryConditionOperator.NE: return res != 0;
                    case QueryConditionOperator.G: return res > 0;
                    case QueryConditionOperator.GE: return res >= 0;
                    case QueryConditionOperator.L: return res < 0;
                    case QueryConditionOperator.LE: return res <= 0;
                }
            }
            
            return false;
        }

        private string OperatorString()
        {
            switch (Operator)
            {
                case QueryConditionOperator.G:
                    return ">";
                case QueryConditionOperator.GE:
                    return ">=";
                case QueryConditionOperator.E:
                    return "=";
                case QueryConditionOperator.L:
                    return "<";
                case QueryConditionOperator.LE:
                    return "<=";
                case QueryConditionOperator.NE:
                    return "!=";
            }
            return null;
        }
        private string ValueString()
        {
            if (Value is DateTime)
            {
                return "'" + ((DateTime)Value).ToString("yyyy-MM-dd HH:mm:ss") + "'";
            }
            if (Value is double)
            {
                return ((double)Value).ToString(CultureInfo.InvariantCulture);
            }
            return Value.ToString();
        }
        public override string ToString()
        {
            return "(" + Column + " " + OperatorString() + " " + ValueString() + ")";
        }
    }

    public enum QueryConditionOperator
    {
        G,
        GE,
        E,
        L,
        LE,
        NE,
    }
}
