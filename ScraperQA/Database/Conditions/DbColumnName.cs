﻿namespace ScraperQA.Database.Conditions
{
    public class DbColumnName
    {
        public string Name { get; set; }

        public DbColumnName()
        {
        }

        public DbColumnName(string name)
        {
            Name = name;
        }

        public override string ToString()
        {
            return this.Name;
        }

        public static Condition operator >(DbColumnName l, object r)
        {
            return new QueryCondition() { Column = l, Operator = QueryConditionOperator.G, Value = r };
        }
        public static Condition operator <(DbColumnName l, object r)
        {
            return new QueryCondition() { Column = l, Operator = QueryConditionOperator.L, Value = r };
        }
        public static Condition operator <=(DbColumnName l, object r)
        {
            return new QueryCondition() { Column = l, Operator = QueryConditionOperator.LE, Value = r };
        }
        public static Condition operator >=(DbColumnName l, object r)
        {
            return new QueryCondition() { Column = l, Operator = QueryConditionOperator.GE, Value = r };
        }
        public static Condition operator ==(DbColumnName l, object r)
        {
            return new QueryCondition() { Column = l, Operator = QueryConditionOperator.E, Value = r };
        }
        public static Condition operator !=(DbColumnName l, object r)
        {
            return new QueryCondition() { Column = l, Operator = QueryConditionOperator.NE, Value = r };
        }
        public static implicit operator DbColumnName(string s)
        {
            return new DbColumnName(s);
        }
    }
}
