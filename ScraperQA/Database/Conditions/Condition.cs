﻿using System.Collections.Generic;

namespace ScraperQA.Database.Conditions
{
    public abstract class Condition
    {
        public abstract bool IsValid(object obj);
        public static Condition operator &(Condition l, Condition r)
        {
            return new AndCondition() { Left = l, Right = r };
        }
        public static Condition operator |(Condition l, Condition r)
        {
            return new OrCondition() { Left = l, Right = r };
        }
        public static Condition operator !(Condition v)
        {
            return new NotCondition() { Value = v };
        }

        public IEnumerable<T> Filter<T>(IEnumerable<T> stream)
        {
            foreach (var t in stream)
            {
                if (IsValid(t))
                    yield return t;
            }
        }
        public static IEnumerable<T> Filter<T>(IEnumerable<T> stream, Condition condition)
        {
            if (condition == null)
            {
                foreach (var t in stream)
                    yield return t;
            }
            else
            {
                foreach (var t in stream)
                {
                    if (condition.IsValid(t))
                        yield return t;
                }
            }
        }

    }
}
