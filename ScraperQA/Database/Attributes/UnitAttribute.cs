﻿namespace ScraperQA.Database.Attributes
{
    public class UnitAttribute : System.Attribute
    {
        public string Name { get; private set; }

        public UnitAttribute(string name)
        {
            Name = name;
        }
    }
}
