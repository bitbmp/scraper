﻿namespace ScraperQA.Database.Attributes
{
    public class DescriptionAttribute : System.Attribute
    {
        public string Name { get; private set; }

        public DescriptionAttribute(string name)
        {
            Name = name;
        }
    }
}
