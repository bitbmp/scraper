﻿namespace ScraperQA.Database.Attributes
{
    class DbTableName : System.Attribute
    {
        public string Name { get; private set; }

        public DbTableName(string name)
        {
            Name = name;
        }
    }
}
