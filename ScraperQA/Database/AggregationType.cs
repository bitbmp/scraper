﻿namespace ScraperQA.Database
{
    public enum AggregationType
    {
        Average = 1,
        Minimum = 2,
        Maximum = 3,
        Instantaneous = 4,
        Accumulation = 5,
    }
}
