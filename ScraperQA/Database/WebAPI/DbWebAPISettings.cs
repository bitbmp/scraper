﻿using Newtonsoft.Json;

namespace ScraperQA.Database.WebAPI
{
    public class DbWebAPISettings
    {
        [JsonProperty("ip_address")]
        public string IPAddress { get; set; }
        [JsonProperty("port")]
        public string Port { get; set; }
    }
}
