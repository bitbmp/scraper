﻿using Newtonsoft.Json;
using NHttp;
using ScraperQA.Utility.Visualization;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;

namespace ScraperQA.Database.WebAPI
{
    public class DbWebAPI
    {
        private HttpServer server;
        private DatabaseManager db;
        public DbWebAPI(DatabaseManager db, DbWebAPISettings settings)
        {
            this.db = db;
            server = new HttpServer();
            server.RequestReceived += Server_RequestReceived;
            server.EndPoint = new IPEndPoint(IPAddress.Parse(settings.IPAddress), int.Parse(settings.Port));
            server.Start();
        }

        private void Server_RequestReceived(object sender, HttpRequestEventArgs e)
        {
            if (e.Request.Path == "/records.stream")
            {
                HandleRecordsStream(e);
            }
            else if (e.Request.Path == "/records.json")
            {
                HandleRecordsJson(e, Formatting.Indented);
            }
            else if (e.Request.Path == "/records.json.min")
            {
                HandleRecordsJson(e, Formatting.None);
            }
            else if (e.Request.Path == "/marker.html")
            {
                HandleMarkerVisualization(e);
            }
            else
            {
                e.Response.StatusCode = 404;
                e.Response.Status = "Error 404";
                e.Response.StatusDescription = e.Request.Path + " not found.";
                StreamWriter writer = new StreamWriter(e.Response.OutputStream);
                writer.Write(File.ReadAllText("dataviewer\\404.html"));
                writer.Close();
            }
        }
        private string GetQuery(HttpRequestEventArgs e)
        {
            string q = e.Request.Params["q"];
            if (string.IsNullOrEmpty(q))
            {
                StreamWriter writer = new StreamWriter(e.Response.OutputStream);
                writer.Write("Invalid query (q)");
                writer.Close();
                return null;
            }
            return q;
        }
        private void HandleRecordsJson(HttpRequestEventArgs e, Formatting format)
        {
            string q = GetQuery(e);
            if (q == null) return;

            StreamWriter writer = new StreamWriter(e.Response.OutputStream);
            var res = db.SelectQ<Record>(q, null);
            writer.Write(JsonConvert.SerializeObject(res.ToList(), format));
            writer.Close();
        }
        private void HandleMarkerVisualization(HttpRequestEventArgs e)
        {
            string q = GetQuery(e);
            if (q == null) return;

            StreamWriter writer = new StreamWriter(e.Response.OutputStream);
            var res = db.SelectQ<Record>(q, null);
            string data = GenerateMarkerData(db, res);
            data = File.ReadAllText("dataviewer\\index.html").Replace("##DATA##", data);
            writer.Write(data);
            writer.Close();

        }
        private void HandleRecordsStream(HttpRequestEventArgs e)
        {
            string q = GetQuery(e);
            if (q == null) return;
            using (GZipStream gzstream = new GZipStream(e.Response.OutputStream, CompressionLevel.Optimal))
            {
                var res = db.SelectQ<Record>(q, null);
                foreach (Record r in res)
                {
                    gzstream.Write(BitConverter.GetBytes(r.Date.Ticks), 0, 8);
                    gzstream.Write(BitConverter.GetBytes(r.Span.Ticks), 0, 8);
                    gzstream.Write(BitConverter.GetBytes(r.Latitude), 0, 8);
                    gzstream.Write(BitConverter.GetBytes(r.Longitude), 0, 8);
                    gzstream.Write(BitConverter.GetBytes(r.Altitude), 0, 8);
                    gzstream.Write(BitConverter.GetBytes(r.Value), 0, 8);
                    gzstream.Write(BitConverter.GetBytes(r.ValueType), 0, 4);
                    gzstream.Write(BitConverter.GetBytes(r.AggregationType), 0, 4);
                    if (r.Source != null)
                    {
                        gzstream.Write(BitConverter.GetBytes((int)r.Source), 0, 4);
                    }
                    else
                    {
                        gzstream.Write(BitConverter.GetBytes((int)-1), 0, 4);
                    }
                }
            }
        }

        public void Close()
        {
            server.Dispose();
        }

        static string GenerateMarkerData(DatabaseManager db, IEnumerable<Record> records)
        {
            var result = records.GroupBy(x => x.Source);
            var vType = db.Select<DbValueType>().ToDictionary(x => x.Id);
            var aType = db.Select<DbAggregationType>().ToDictionary(x => x.Id);
            var stations = db.Select<DbStation>().ToDictionary(x => x.Id);

            List<Marker> markers = new List<Marker>();
            foreach (var res in result)
            {
                Marker m = new Marker();
                if (stations.ContainsKey((int)res.Key))
                {
                    m.Title = stations[(int)res.Key].Name;
                    m.Position = new MarkerPosition() { Latitude = (double)stations[(int)res.Key].Latitude, Longitude = (double)stations[(int)res.Key].Longitude };
                }
                else
                {
                    m.Title = "[unknown]";
                    m.Position = null;
                }
                m.Label = res.Count().ToString();
                m.Values = new List<MarkerValue>();
                foreach (Record r in res)
                {
                    m.Values.Add(new MarkerValue()
                    {
                        Name = vType[r.ValueType].Name,
                        Type = aType[r.AggregationType].Name,
                        Start = r.Date.ToString("yyyy-MM-dd HH:mm:ss"),
                        End = r.Date.Add(r.Span).ToString("yyyy-MM-dd HH:mm:ss"),
                        Unit = vType[r.ValueType].Unit,
                        Value = r.Value,
                    });
                    if (m.Position == null)
                    {
                        m.Position = new MarkerPosition() { Latitude = r.Latitude, Longitude = r.Longitude };
                    }
                }
                markers.Add(m);
            }

            string data = JsonConvert.SerializeObject(markers, Formatting.Indented);
            return data;
        }
    }
}
