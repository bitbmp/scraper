﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Npgsql;
using ScraperQA.Database.Attributes;
using ScraperQA.Utility.Files;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace ScraperQA.Database
{
    [DbTableName("aq_station")]
    public partial class DbStation : IInsertable, ISelectable, ICSVWritable
    { 
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("latitude")]
        public double Latitude { get; set; }

        [JsonProperty("longitude")]
        public double Longitude { get; set; }

        [JsonProperty("altitude")]
        public double Altitude { get; set; }

        public DbStation()
        {

        }
        public DbStation(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            DbStation r = (DbStation)obj;
            return this.Id == r.Id;
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + Id.GetHashCode();
                return hash;
            }
        }

        public override string ToString()
        {
            return String.Format("DbStation[Id: {0}, Name: {1}, Lat: {2}, Lon: {3}, Alt: {4}]",
                Id, Name, Latitude, Longitude, Altitude);
        }

        public virtual NpgsqlCommand GetNpgsqlCommand()
        {
            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = @"INSERT INTO aq_station (id, name, latitude, longitude, altitude) VALUES (@id, @na, @la, @lo, @al)";
            cmd.Parameters.AddWithValue("id", this.Id);
            cmd.Parameters.AddWithValue("na", this.Name);
            cmd.Parameters.AddWithValue("la", this.Latitude);
            cmd.Parameters.AddWithValue("lo", this.Longitude);
            cmd.Parameters.AddWithValue("al", this.Altitude);
            return cmd;
        }

        public virtual void SetFromReader(NpgsqlDataReader reader)
        {
            Id = reader.GetInt32(0);
            Name = reader.IsDBNull(1) ? null : reader.GetString(1);
            Latitude = reader.GetDouble(2);
            Longitude = reader.GetDouble(3);
            Altitude = reader.GetDouble(4);
        }

        public IEnumerable<string> GetCSVHeaders()
        {
            return new List<string>
            {
                "Latitude",
                "Longitude",
                "Name"
            };
        }

        public IEnumerable<string> GetCSVValues()
        {
            return new List<string>
            {
                Latitude.ToString(),
                Longitude.ToString(),
                String.IsNullOrWhiteSpace(Name) ? "" : Name.ToString()
            };
        }
    }



    public partial class DbStation
    {
        public static List<DbStation> FromJson(string json) => JsonConvert.DeserializeObject<List<DbStation>>(json, Converter.Settings);
    }

    public static partial class Serialize
    {
        public static string ToJson(this List<DbStation> self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal partial class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
            new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
        },
        };
    }

}
