﻿using Npgsql;
using NpgsqlTypes;
using PostgreSQLCopyHelper;
using ScraperQA.Database.Attributes;
using ScraperQA.Database.Conditions;
using ScraperQA.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ScraperQA.Database
{
    public class DatabaseManager
    {
        private string connectionString;

        public DatabaseManager(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<Record> SmartInsert(IEnumerable<Record> records, int bulkSize = 1000)
        {
            List<Record> duplicate = new List<Record>();
            List<Record> buffer = new List<Record>(bulkSize);
            int c = 0;

            foreach (var r in records)
            {
                buffer.Add(r);
                c++;
                if (c >= bulkSize)
                {
                    try
                    {
                        BulkInsert(buffer);
                    }
                    catch
                    {
                        duplicate.AddRange(Insert<Record>(buffer));
                    }
                    c = 0;
                    buffer.Clear();
                }
            }
            if (buffer.Count != 0)
            {
                try
                {
                    BulkInsert(buffer);
                }
                catch
                {
                    duplicate.AddRange(Insert<Record>(buffer));
                }
            }

            return duplicate;
        }
        public bool Insert(IInsertable record)
        {
            return Insert(new IInsertable[] { record }).Count == 0;
        }
        public void BulkInsert(IEnumerable<Record> records, int size = -1)
        {
            Logger.Log("Bulk insert started", LogType.Info, false);
            PostgreSQLCopyHelper<Record> a = new PostgreSQLCopyHelper<Record>("public", "aq_values")
                    .Map("date", x => x.Date, NpgsqlDbType.Timestamp)
                    .Map("span", x => x.Span.TotalSeconds, NpgsqlDbType.Integer)
                    .Map("latitude", x => x.Latitude, NpgsqlDbType.Numeric)
                    .Map("longitude", x => x.Longitude, NpgsqlDbType.Numeric)
                    .Map("altitude", x => x.Altitude, NpgsqlDbType.Numeric)
                    .Map("value", x => x.Value, NpgsqlDbType.Double)
                    .Map("value_type", x => x.ValueType, NpgsqlDbType.Integer)
                    .Map("aggregation_type", x => x.AggregationType, NpgsqlDbType.Integer)
                    .Map("source", x => x.Source, NpgsqlDbType.Integer);
            ulong count;
            using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                count = a.SaveAll(connection, records);
            }
            Logger.Log("Bulk insert finished (" + count + "/" + size + ") row", LogType.Info, false);
        }

        public void BulkInsertInBatches(IEnumerable<Record> records, int batchSize = 200000)
        {
            IEnumerable<Record> unique_records = records.Distinct();
            var batches = unique_records.Batch(batchSize);
            foreach(IEnumerable<Record> batch in batches)
            {
                List<Record> toBeInserted = batch.ToList();
                try
                {
                    BulkInsert(toBeInserted, toBeInserted.Count);
                } catch(Exception ex)
                {
                    Logger.Log(String.Format("Error inserting batch: {0}", ex.Message), LogType.Warning);
                    Insert(toBeInserted);
                }
            }
        }

        public List<T> Insert<T>(IEnumerable<T> records) where T : IInsertable
        {
            List<T> failedInserts = new List<T>();
            NpgsqlConnection connection = new NpgsqlConnection(connectionString);
            connection.Open();

            foreach (T r in records)
            {
                NpgsqlCommand cmd = r.GetNpgsqlCommand();
                cmd.Connection = connection;

                try
                {
                    cmd.ExecuteNonQuery();
                    //Logger.Log(r + " inserted correctly", LogType.Info, false);
                }
                catch (Exception ex)
                {
                    failedInserts.Add(r);
                    Logger.Log(r + " insertion failded: " + ex.Message, LogType.Error, true);
                }
                cmd.Dispose();
            }

            connection.Close();
            return failedInserts;
        }
        public IEnumerable<T> SelectQ<T>(string condition = null, ulong? limit = null) where T : ISelectable
        {
            Type t = typeof(T);
            DbTableName tableName;
            try
            {
                tableName = (DbTableName)t.GetCustomAttribute(typeof(DbTableName));
            }
            catch
            {
                throw new Exception("Invalid T, must have DbTableName attribute");
            }
            NpgsqlConnection connection = new NpgsqlConnection(connectionString);
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "SELECT * FROM " + tableName.Name + " " + (condition != null ? (" WHERE " + condition) : "")
                + (limit != null ? ("LIMIT " + limit) : "");

            Logger.Log("Executing: " + cmd.CommandText, LogType.Info, false);
            DateTime start = DateTime.Now;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            Logger.Log("Done (" + Math.Round((DateTime.Now - start).TotalSeconds, 2) + " s)", LogType.Info, false);

            while (reader.Read())
            {
                T record = (T)Activator.CreateInstance(typeof(T));
                record.SetFromReader(reader);
                yield return record;
            }

            connection.Close();
        }
        public IEnumerable<T> Select<T>(Condition condition = null, ulong? limit = null) where T : ISelectable
        {
            var res = SelectQ<T>(condition == null ? null : condition.ToString(), limit);
            foreach (var r in res)
            {
                yield return r;
            }
        }

        public IEnumerable<Record> Read(Condition condition)
        {
            NpgsqlConnection connection = new NpgsqlConnection(connectionString);
            connection.Open();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "SELECT * FROM aq_values" + (condition != null ? (" WHERE " + condition) : "");

            Logger.Log("Executing: " + cmd.CommandText, LogType.Info, false);
            DateTime start = DateTime.Now;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            Logger.Log("Done (" + Math.Round((DateTime.Now - start).TotalSeconds, 2) + " s)", LogType.Info, false);

            while (reader.Read())
            {
                Record record = new Record()
                {
                    Date = (DateTime)reader[0],
                    Span = TimeSpan.FromSeconds((int)reader[1]),

                    Latitude = (double)reader[2],
                    Longitude = (double)reader[3],
                    Altitude = (double)reader[4],

                    Value = (double)reader[5],
                    ValueType = (int)reader[6],
                    AggregationType = (int)reader[7],

                    Source = (int?)reader[8],
                };
                yield return record;
            }

            connection.Close();
        }
    }
}
