﻿using Npgsql;

namespace ScraperQA.Database
{
    public interface IInsertable
    {
        NpgsqlCommand GetNpgsqlCommand();
    }
}
