﻿using Newtonsoft.Json;
using Npgsql;
using ScraperQA.Database.Attributes;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace ScraperQA.Database
{
    [DbTableName("aq_value_type")]
    public partial class DbValueType : IInsertable, ISelectable
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("unit")]
        public string Unit { get; set; } = null;

        public DbValueType()
        {

        }
        public DbValueType(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            DbValueType r = (DbValueType)obj;
            return this.Id == r.Id;
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + Id.GetHashCode();
                return hash;
            }
        }

        public override string ToString()
        {
            return String.Format("DbValueType[Id: {0}, Name: {1}, Unit: {2}]",
                Id, Name, Unit);
        }

        public NpgsqlCommand GetNpgsqlCommand()
        {
            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = @"INSERT INTO aq_value_type (id, name, unit) VALUES (@id, @na, @un)";
            cmd.Parameters.AddWithValue("id", this.Id);
            cmd.Parameters.AddWithValue("na", this.Name);
            cmd.Parameters.AddWithValue("un", this.Unit);
            return cmd;
        }

        public static IEnumerable<DbValueType> GetDbValueTypes()
        {
            foreach (ValueType v in Enum.GetValues(typeof(ValueType)))
            {
                FieldInfo fi = v.GetType().GetField(v.ToString());
                DescriptionAttribute[] des = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
                string description = des != null && des.Length > 0 ? des[0].Name : v.ToString();
                UnitAttribute[] un = (UnitAttribute[])fi.GetCustomAttributes(typeof(UnitAttribute), false);
                string unit = un != null && un.Length > 0 ? un[0].Name : null;

                DbValueType vl = new DbValueType((int)v, description) { Unit = unit };

                yield return vl;
            }
        }

        public void SetFromReader(NpgsqlDataReader reader)
        {
            Id = reader.GetInt32(0);
            Name = reader.GetString(1);
            Unit = reader.GetString(2);
        }
    }

    

    public partial class DbValueType
    {
        public static List<DbValueType> FromJson(string json) => JsonConvert.DeserializeObject<List<DbValueType>>(json, Converter.Settings);
    }

    public static partial class Serialize
    {
        public static string ToJson(this List<DbValueType> self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }
}
