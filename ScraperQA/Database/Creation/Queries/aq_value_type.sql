CREATE TABLE IF NOT EXISTS public.aq_value_type
(
    id integer NOT NULL,
    name character varying(64) COLLATE pg_catalog."default" NOT NULL,
    unit character varying(64) COLLATE pg_catalog."default",
    CONSTRAINT aq_value_type_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.aq_value_type
    OWNER to @tbowner;