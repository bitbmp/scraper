CREATE TABLE IF NOT EXISTS public.aq_values
(
    date timestamp without time zone NOT NULL,
    span integer NOT NULL,
    latitude numeric(8,6) NOT NULL,
    longitude numeric(9,6) NOT NULL,
    altitude numeric(8,2) NOT NULL,
    value double precision NOT NULL,
    value_type integer NOT NULL,
    aggregation_type integer NOT NULL,
    source integer,
    CONSTRAINT aq_values_pkey PRIMARY KEY (date, span, latitude, longitude, altitude, aggregation_type, value_type)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.aq_values
    OWNER to @tbowner;