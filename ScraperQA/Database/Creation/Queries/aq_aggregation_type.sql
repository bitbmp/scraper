CREATE TABLE IF NOT EXISTS public.aq_aggregation_type
(
    id integer NOT NULL,
    name character varying(16) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT aq_aggregation_type_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.aq_aggregation_type
    OWNER to @tbowner;