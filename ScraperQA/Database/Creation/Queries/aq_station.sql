CREATE TABLE IF NOT EXISTS public.aq_station
(
    id integer NOT NULL,
    name character varying(64) COLLATE pg_catalog."default",
    latitude numeric(8,6) NOT NULL,
    longitude numeric(9,6) NOT NULL,
    altitude double precision,
    CONSTRAINT aq_station_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.aq_station
    OWNER to @tbowner;