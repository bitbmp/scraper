CREATE DATABASE @dbname
    WITH 
    OWNER = @dbowner
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_United States.1252'
    LC_CTYPE = 'English_United States.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

COMMENT ON DATABASE @dbname
    IS 'Database containing air quality data';