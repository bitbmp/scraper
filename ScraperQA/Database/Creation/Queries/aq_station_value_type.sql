CREATE TABLE IF NOT EXISTS public.aq_station_value_type
(
    id_station integer NOT NULL,
    id_value_type integer NOT NULL,
    CONSTRAINT aq_station_value_type_pkey PRIMARY KEY (id_station, id_value_type)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.aq_station_value_type
    OWNER to @tbowner;