﻿using Npgsql;
using System;
using System.IO;

namespace ScraperQA.Database.Creation
{
    public class DbCreator
    {
        public string Host { get; private set; }
        public string Username { get; private set; }
        public string Password { get; private set; }
        public string Database { get; private set; }

        public string ConnectionString1 { get; private set; }
        public string ConnectionString2 { get; private set; }

        public DbCreator(string host, string username, string password, string database)
        {
            Host = host;
            Username = username;
            Password = password;
            Database = database;
            ConnectionString1 = String.Format("Host={0};Username={1};Password={2};Database={3};", Host, Username, Password, "postgres");
            ConnectionString2 = String.Format("Host={0};Username={1};Password={2};Database={3};", Host, Username, Password, Database);
        }

        public void Execute()
        {
            string query = null;
            NpgsqlConnection conn = new NpgsqlConnection(ConnectionString1);

            NpgsqlCommand cmd1 = new NpgsqlCommand();
            cmd1.Connection = conn;

            cmd1.CommandText = @"SELECT COUNT(*)
                                FROM pg_catalog.pg_database
                                WHERE datname = @dbname";
            cmd1.Parameters.AddWithValue("dbname", Database);
            
            conn.Open();
            Int64 exist = (Int64)cmd1.ExecuteScalar();

            if(exist == 0)
            {
                query = File.ReadAllText(@"Database\Creation\Queries\db_creation.sql");
                query = query.Replace("@dbname", Database);
                query = query.Replace("@dbowner", Username);

                cmd1.CommandText = query;
                cmd1.Parameters.Clear();
                cmd1.ExecuteNonQuery();
            }

            conn.Close();

            conn = new NpgsqlConnection(ConnectionString2);

            NpgsqlCommand cmd2 = new NpgsqlCommand();
            NpgsqlCommand cmd3 = new NpgsqlCommand();
            NpgsqlCommand cmd4 = new NpgsqlCommand();
            NpgsqlCommand cmd5 = new NpgsqlCommand();

            cmd1.Connection = conn;
            cmd2.Connection = conn;
            cmd3.Connection = conn;
            cmd4.Connection = conn;
            cmd5.Connection = conn;


            query = File.ReadAllText(@"Database\Creation\Queries\aq_values.sql");
            query = query.Replace("@tbowner", Username);
            cmd1.CommandText = query;

            query = File.ReadAllText(@"Database\Creation\Queries\aq_value_type.sql");
            query = query.Replace("@tbowner", Username);
            cmd2.CommandText = query;

            query = File.ReadAllText(@"Database\Creation\Queries\aq_station.sql");
            query = query.Replace("@tbowner", Username);
            cmd3.CommandText = query;

            query = File.ReadAllText(@"Database\Creation\Queries\aq_aggregation_type.sql");
            query = query.Replace("@tbowner", Username);
            cmd4.CommandText = query;

            query = File.ReadAllText(@"Database\Creation\Queries\aq_station_value_type.sql");
            query = query.Replace("@tbowner", Username);
            cmd5.CommandText = query;

            conn.Open();

            cmd1.ExecuteNonQuery();
            cmd2.ExecuteNonQuery();
            cmd3.ExecuteNonQuery();
            cmd4.ExecuteNonQuery();
            cmd5.ExecuteNonQuery();

            conn.Close();
        }
    }
}
