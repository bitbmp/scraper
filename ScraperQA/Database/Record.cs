﻿using Npgsql;
using ScraperQA.Database.Attributes;
using ScraperQA.Database.Conditions;
using System;

namespace ScraperQA.Database
{
    [DbTableName("aq_values")]
    public struct Record : IInsertable, ISelectable
    {
        public static readonly DbColumnName DATE = "date";
        public static readonly DbColumnName ALTITUDE = "altitude";
        public static readonly DbColumnName LONGITUDE = "longitude";
        public static readonly DbColumnName LATITUDE = "latitude";
        public static readonly DbColumnName SOURCE = "source";
        public static readonly DbColumnName VALUE_TYPE = "value_type";
        public static readonly DbColumnName AGGREGATION_TYPE = "aggregation_type";
        public static readonly DbColumnName SPAN = "span";

        //Time
        public DateTime Date;
        public TimeSpan Span;

        //Space
        public double Latitude;
        public double Longitude;
        public double Altitude;

        //Value
        public double Value;
        public int ValueType;
        public int AggregationType;

        //Source
        public int? Source;

        public override string ToString()
        {
            return String.Format("Record[{0}, {8}s, Lon: {1}, Lat: {2}, Alt: {3}, Val: {4}, Type: {5}, Source: {6}, Aggregation: {7}]",
                Date, Longitude, Latitude, Altitude, Value, ((ValueType)ValueType), Source, ((AggregationType)AggregationType), Span.TotalSeconds);
        }

        public NpgsqlCommand GetNpgsqlCommand()
        {
            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = @"INSERT INTO aq_values (date, span, latitude, longitude, altitude, value, value_type, aggregation_type, source) VALUES (@d, @sp, @la, @lo, @al, @v, @vt, @at, @so)";
            cmd.Parameters.AddWithValue("d", this.Date);
            cmd.Parameters.AddWithValue("sp", this.Span.TotalSeconds);
            cmd.Parameters.AddWithValue("la", this.Latitude);
            cmd.Parameters.AddWithValue("lo", this.Longitude);
            cmd.Parameters.AddWithValue("al", this.Altitude);
            cmd.Parameters.AddWithValue("v", this.Value);
            cmd.Parameters.AddWithValue("vt", this.ValueType);
            cmd.Parameters.AddWithValue("at", this.AggregationType);
            cmd.Parameters.AddWithValue("so", this.Source);
            return cmd;
        }

        public void SetFromReader(NpgsqlDataReader reader)
        {
            Date = (DateTime)reader[0];
            Span = TimeSpan.FromSeconds((int)reader[1]);
            Latitude = reader.GetDouble(2);
            Longitude = reader.GetDouble(3);
            Altitude = reader.GetDouble(4);
            Value = (double)reader[5];
            ValueType = (int)reader[6];
            AggregationType = (int)reader[7];
            Source = (int?)reader[8];
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Record r = (Record)obj;
            return  this.Date == r.Date &&
                    this.Span == r.Span &&
                    this.Latitude == r.Latitude &&
                    this.Longitude == r.Longitude &&
                    this.Altitude == r.Altitude &&
                    this.ValueType == r.ValueType &&
                    this.AggregationType == r.AggregationType;
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + this.Date.GetHashCode();
                hash = hash * 23 + this.Span.GetHashCode();
                hash = hash * 23 + this.Latitude.GetHashCode();
                hash = hash * 23 + this.Longitude.GetHashCode();
                hash = hash * 23 + this.Altitude.GetHashCode();
                hash = hash * 23 + this.ValueType.GetHashCode();
                hash = hash * 23 + this.AggregationType.GetHashCode();
                return hash;
            }
        }
    }
}
