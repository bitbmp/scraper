﻿using ScraperQA.Database.Attributes;

namespace ScraperQA.Database
{
    public enum ValueType : int
    {
        /// <summary>
        /// ug / m3 (microgram / meter^3)
        /// </summary>
        [Description("NO2 (Nitrogen dioxide)"), Unit("ug / m3 (microgram / meter^3)")]
        NO2 = 2,
        /// <summary>
        /// ug / m3 (microgram / meter^3)
        /// </summary>
        [Description("PM10"), Unit("ug / m3 (microgram / meter^3)")]
        PM10 = 10,
        /// <summary>
        /// ug / m3 (microgram / meter^3)
        /// </summary>
        [Description("C6H6 (Benzene)"), Unit("ug / m3 (microgram / meter^3)")]
        C6H6 = 66,
        /// <summary>
        /// ug / m3 (microgram / meter^3)
        /// </summary>
        [Description("CO (Carbon monoxide)"), Unit("ug / m3 (microgram / meter^3)")]
        CO = 0,
        /// <summary>
        /// ug / m3 (microgram / meter^3)
        /// </summary>
        [Description("PM2.5"), Unit("ug / m3 (microgram / meter^3)")]
        PM25 = 25,
        /// <summary>
        /// ug / m3 (microgram / meter^3)
        /// </summary>
        [Description("O3 (Ozone)"), Unit("ug / m3 (microgram / meter^3)")]
        O3 = 3,
        /// <summary>
        /// ug / m3 (microgram / meter^3)
        /// </summary>
        [Description("SO2 (Sulfur dioxide)"), Unit("ug / m3 (microgram / meter^3)")]
        SO2 = 502,
        /// <summary>
        /// ug / m3 (microgram / meter^3)
        /// </summary>
        [Description("NOX (Nitric oxides)"), Unit("ug / m3 (microgram / meter^3)")]
        NOX = 16,
        /// <summary>
        /// ug / m3 (microgram / meter^3)
        /// </summary>
        [Description("C6H5-CH3 (Toluene)"), Unit("ug / m3 (microgram / meter^3)")]
        C6H5CH3 = 653,
        /// <summary>
        /// ug / m3 (microgram / meter^3)
        /// </summary>
        [Description("NO (Nitrogen monoxide)"), Unit("ug / m3 (microgram / meter^3)")]
        NO = 40,
        /// <summary>
        /// ug / m3 (microgram / meter^3)
        /// </summary>
        [Description("o-xylene"), Unit("ug / m3 (microgram / meter^3)")]
        OXYLENE = 82,
        /// <summary>
        /// ug / m3 (microgram / meter^3)
        /// </summary>
        [Description("Black carbon"), Unit("ug / m3 (microgram / meter^3)")]
        BLACK_CARBON = 113,
        /// <summary>
        /// ug / m3 (microgram / meter^3)
        /// </summary>
        [Description("PM1.0"), Unit("ug / m3 (microgram / meter^3)")]
        PM1 = 631,

        /// <summary>
        /// K (kelvin)
        /// </summary>
        [Unit("K (kelvin)")]
        Temperature = 1000,
        /// <summary>
        /// mm (millimeter)
        /// </summary>
        [Unit("mm (millimeter)")]
        Precipitation = 1001,
        /// <summary>
        /// hPa (hectopascal)
        /// </summary>
        [Unit("hPa (hectopascal)")]
        Pressure = 1002,
        /// <summary>
        /// % (percentage)
        /// </summary>
        [Unit("% (percentage)")]
        Humidity = 1003,
        /// <summary>
        /// m/s (meter / second)
        /// </summary>
        [Description("Wind speed"), Unit("m/s (meter / second)")]
        WindSpeed = 1004,
        /// <summary>
        /// degrees (meteorological)
        /// </summary>
        [Description("Wind direction"), Unit("degrees (meteorological)")]
        WindDirection = 1005,
        /// <summary>
        /// %
        /// </summary>
        [Unit("mm (millimeter)")]
        Cloudiness = 1006,
        /// <summary>
        /// mm or kg/m2 (millimeter or kilogram / meter ^ 2)
        /// </summary>
        [Description("Snow precipitation"), Unit("mm or kg/m2 (millimeter or kilogram / meter ^ 2)")]
        SnowPrecipitation = 1007,
        /// <summary>
        /// w/m2
        /// </summary>
        [Description("Visible radiation"), Unit("w/m2")]
        VisibleRadiation = 1008,
        /// <summary>
        /// j/m2
        /// </summary>
        [Description("Global solar radiation"), Unit("j/m2")]
        GlobalSolarRadiation = 1009,

    }
}
