﻿using Newtonsoft.Json;
using Npgsql;
using ScraperQA.Database.Attributes;
using System;
using System.Collections.Generic;

namespace ScraperQA.Database
{
    [DbTableName("aq_aggregation_type")]
    public partial class DbAggregationType : IInsertable, ISelectable
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        public DbAggregationType()
        {

        }
        public DbAggregationType(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            DbAggregationType r = (DbAggregationType)obj;
            return this.Id == r.Id;
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + Id.GetHashCode();
                return hash;
            }
        }

        public override string ToString()
        {
            return String.Format("DbAggregationType[Id: {0}, Name: {1}]",
                Id, Name);
        }

        public NpgsqlCommand GetNpgsqlCommand()
        {
            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.CommandText = @"INSERT INTO aq_aggregation_type (id, name) VALUES (@id, @na)";
            cmd.Parameters.AddWithValue("id", this.Id);
            cmd.Parameters.AddWithValue("na", this.Name);
            return cmd;
        }

        public static IEnumerable<DbAggregationType> GetDbAggregationTypes()
        {
            foreach (AggregationType v in Enum.GetValues(typeof(AggregationType)))
            {
                DbAggregationType vl = new DbAggregationType((int)v, v.ToString());
                yield return vl;
            }
        }

        public void SetFromReader(NpgsqlDataReader reader)
        {
            Id = reader.GetInt32(0);
            Name = reader.GetString(1);
        }
    }

    public partial class DbAggregationType
    {
        public static List<DbAggregationType> FromJson(string json) => JsonConvert.DeserializeObject<List<DbAggregationType>>(json, Converter.Settings);
    }

    public static partial class Serialize
    {
        public static string ToJson(this List<DbAggregationType> self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }
}
