﻿using Npgsql;

namespace ScraperQA.Database
{
    public interface ISelectable
    {
        void SetFromReader(NpgsqlDataReader reader);
    }
}
