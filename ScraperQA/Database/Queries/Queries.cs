﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Npgsql;
using ScraperQA.Database.Attributes;
using ScraperQA.Utility;

namespace ScraperQA.Database.Queries
{
    public static class Queries
    {
        [DbTableName("aq_station INNER JOIN aq_station_value_type ON aq_station_value_type.id_station = aq_station.id")]
        public class StationParameterPair : DbStation
        {
            [JsonProperty("value_type")]
            public ValueType? ValueType { get; set; } = null;

            public StationParameterPair() 
                : base()
            {
            }

            public StationParameterPair(DbStation s, ValueType vt)
            {
                this.Id = s.Id;
                this.Latitude = s.Latitude;
                this.Longitude = s.Longitude;
                this.Name = s.Name;
                this.Altitude = s.Altitude;
                this.ValueType = vt;
            }

            public override void SetFromReader(NpgsqlDataReader reader)
            {
                base.SetFromReader(reader);
                ValueType = (ValueType)reader.GetInt32(6);
            }

            public override NpgsqlCommand GetNpgsqlCommand()
            {
                NpgsqlCommand cmd = new NpgsqlCommand();
                cmd.CommandText = @"INSERT INTO aq_station_value_type(id_station, id_value_type) VALUES (@id_station, @id_value_type)";
                cmd.Parameters.AddWithValue("id_station", this.Id);
                cmd.Parameters.AddWithValue("id_value_type", (int)this.ValueType);
                return cmd;
            }

            public override string ToString()
            {
                return String.Format("DbStation[Id: {0}, Name: {1}, Lat: {2}, Lon: {3}, Alt: {4}, ValType: {5}]",
                    Id, Name, Latitude, Longitude, Altitude, ValueType);
            }
        }

        public static void UpdateAqStationValueTypeTable()
        {
            ExecuteNonQueryFromFile(@"Database\Queries\UpdateStationsValueTypes.sql");
        }

        public static void UpdateStations()
        {
            ExecuteNonQueryFromFile(@"Database\Queries\UpdateStations.sql");
        }

        public static void ExecuteNonQueryFromFile(string path)
        {
            Settings settings = JsonConvert.DeserializeObject<Settings>(File.ReadAllText("settings.json"));
            NpgsqlConnection conn = new NpgsqlConnection(String.Format("Host={0};Username={1};Password={2};Database={3};",
                settings.Db.Host, settings.Db.Username, settings.Db.Password, settings.Db.Database));

            NpgsqlCommand cmd1 = new NpgsqlCommand();
            cmd1.Connection = conn;

            cmd1.CommandText = File.ReadAllText(path);

            conn.Open();

            cmd1.ExecuteNonQuery();

            conn.Close();

        }
    }
}
