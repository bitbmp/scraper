SELECT aq_station.id, aq_station.name, aq_station.latitude, aq_station.longitude, aq_station.altitude, string_agg(aq_value_type.name, ', ')
FROM aq_station LEFT OUTER JOIN aq_station_value_type ON aq_station.id = aq_station_value_type.id_station
LEFT OUTER JOIN aq_value_type ON aq_value_type.id = aq_station_value_type.id_value_type
GROUP BY aq_station.id, aq_station.name, aq_station.latitude, aq_station.longitude, aq_station.altitude;