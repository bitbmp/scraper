﻿using Newtonsoft.Json;
using ScraperQA.Database.WebAPI;

namespace ScraperQA.Database
{
    public class DbSettings
    {
        [JsonProperty("host")]
        public string Host { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("database")]
        public string Database { get; set; }

        [JsonProperty("web_api")]
        public DbWebAPISettings DbWebAPI { get; set; }
    }
}
