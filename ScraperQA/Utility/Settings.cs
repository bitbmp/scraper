﻿using Newtonsoft.Json;
using ScraperQA.ArpaeScraper.AQ_csv;
using ScraperQA.ArpaeScraper.RFC_rmap;
using ScraperQA.Database;
using ScraperQA.OpenWeatherMap;

namespace ScraperQA.Utility
{
    public class Settings
    {
        [JsonProperty("owm")]
        public OwmSettings Owm { get; set; }

        [JsonProperty("db")]
        public DbSettings Db { get; set; }

        [JsonProperty("rmap")]
        public RmapSettings Rmap { get; set; }

        [JsonProperty("aqcsv")]
        public AQCSVSettings Aqcsv { get; set; }
    }
}
