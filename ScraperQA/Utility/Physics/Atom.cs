﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScraperQA.Utility.Physics
{
    public class Atom
    {
        #region All atoms
        public static readonly Atom Actinium = new Atom("Actinium", "Ac", 89, 227.0278);
        public static readonly Atom Aluminum = new Atom("Aluminum", "Al", 13, 26.981539);
        public static readonly Atom Americium = new Atom("Americium", "Am", 95, 243.0614);
        public static readonly Atom Antimony = new Atom("Antimony", "Sb", 51, 121.760);
        public static readonly Atom Argon = new Atom("Argon", "Ar", 18, 39.948);
        public static readonly Atom Arsenic = new Atom("Arsenic", "As", 33, 74.92159);
        public static readonly Atom Astatine = new Atom("Astatine", "At", 85, 209.9871);
        public static readonly Atom Barium = new Atom("Barium", "Ba", 56, 137.327);
        public static readonly Atom Berkelium = new Atom("Berkelium", "Bk", 97, 247.0703);
        public static readonly Atom Beryllium = new Atom("Beryllium", "Be", 4, 9.012182);
        public static readonly Atom Bismuth = new Atom("Bismuth", "Bi", 83, 208.98037);
        public static readonly Atom Boron = new Atom("Boron", "B", 5, 10.811);
        public static readonly Atom Bromine = new Atom("Bromine", "Br", 35, 79.904);
        public static readonly Atom Cadmium = new Atom("Cadmium", "Cd", 48, 112.411);
        public static readonly Atom Calcium = new Atom("Calcium", "Ca", 20, 40.078);
        public static readonly Atom Californium = new Atom("Californium", "Cf", 98, 251.0796);
        public static readonly Atom Carbon = new Atom("Carbon", "C", 6, 12.011);
        public static readonly Atom Cerium = new Atom("Cerium", "Ce", 58, 140.115);
        public static readonly Atom Cesium = new Atom("Cesium", "Cs", 55, 132.90543);
        public static readonly Atom Chlorine = new Atom("Chlorine", "Cl", 17, 35.4527);
        public static readonly Atom Chromium = new Atom("Chromium", "Cr", 24, 51.9961);
        public static readonly Atom Cobalt = new Atom("Cobalt", "Co", 27, 58.93320);
        public static readonly Atom Copper = new Atom("Copper", "Cu", 29, 63.546);
        public static readonly Atom Curium = new Atom("Curium", "Cm", 96, 247.0703);
        public static readonly Atom Dysprosium = new Atom("Dysprosium", "Dy", 66, 162.50);
        public static readonly Atom Einsteinium = new Atom("Einsteinium", "Es", 99, 252.083);
        public static readonly Atom Erbium = new Atom("Erbium", "Er", 68, 167.26);
        public static readonly Atom Europium = new Atom("Europium", "Eu", 63, 151.965);
        public static readonly Atom Fermium = new Atom("Fermium", "Fm", 100, 257.0951);
        public static readonly Atom Fluorine = new Atom("Fluorine", "F", 9, 18.9984032);
        public static readonly Atom Francium = new Atom("Francium", "Fr", 87, 223.0197);
        public static readonly Atom Gadolinium = new Atom("Gadolinium", "Gd", 64, 157.25);
        public static readonly Atom Gallium = new Atom("Gallium", "Ga", 31, 69.723);
        public static readonly Atom Germanium = new Atom("Germanium", "Ge", 32, 72.61);
        public static readonly Atom Gold = new Atom("Gold", "Au", 79, 196.96654);
        public static readonly Atom Hafnium = new Atom("Hafnium", "Hf", 72, 178.49);
        public static readonly Atom Helium = new Atom("Helium", "He", 2, 4.002602);
        public static readonly Atom Holmium = new Atom("Holmium", "Ho", 67, 164.93032);
        public static readonly Atom Hydrogen = new Atom("Hydrogen", "H", 1, 1.00794);
        public static readonly Atom Indium = new Atom("Indium", "In", 49, 114.818);
        public static readonly Atom Iodine = new Atom("Iodine", "I", 53, 126.90447);
        public static readonly Atom Iridium = new Atom("Iridium", "Ir", 77, 192.217);
        public static readonly Atom Iron = new Atom("Iron", "Fe", 26, 55.845);
        public static readonly Atom Krypton = new Atom("Krypton", "Kr", 36, 83.80);
        public static readonly Atom Lanthanum = new Atom("Lanthanum", "La", 57, 138.9055);
        public static readonly Atom Lawrencium = new Atom("Lawrencium", "Lr", 103, 262.11);
        public static readonly Atom Lead = new Atom("Lead", "Pb", 82, 207.2);
        public static readonly Atom Lithium = new Atom("Lithium", "Li", 3, 6.941);
        public static readonly Atom Lutetium = new Atom("Lutetium", "Lu", 71, 174.967);
        public static readonly Atom Magnesium = new Atom("Magnesium", "Mg", 12, 24.3050);
        public static readonly Atom Manganese = new Atom("Manganese", "Mn", 25, 54.93805);
        public static readonly Atom Mendelevium = new Atom("Mendelevium", "Md", 101, 258.10);
        public static readonly Atom Mercury = new Atom("Mercury", "Hg", 80, 200.59);
        public static readonly Atom Molybdenum = new Atom("Molybdenum", "Mo", 42, 95.94);
        public static readonly Atom Neodymium = new Atom("Neodymium", "Nd", 60, 144.24);
        public static readonly Atom Neon = new Atom("Neon", "Ne", 10, 20.1797);
        public static readonly Atom Neptunium = new Atom("Neptunium", "Np", 93, 237.0482);
        public static readonly Atom Nickel = new Atom("Nickel", "Ni", 28, 58.6934);
        public static readonly Atom Niobium = new Atom("Niobium", "Nb", 41, 92.90638);
        public static readonly Atom Nitrogen = new Atom("Nitrogen", "N", 7, 14.00674);
        public static readonly Atom Nobelium = new Atom("Nobelium", "No", 102, 259.1009);
        public static readonly Atom Osmium = new Atom("Osmium", "Os", 76, 190.23);
        public static readonly Atom Oxygen = new Atom("Oxygen", "O", 8, 15.9994);
        public static readonly Atom Palladium = new Atom("Palladium", "Pd", 46, 106.42);
        public static readonly Atom Phosphorus = new Atom("Phosphorus", "P", 15, 30.973762);
        public static readonly Atom Platinum = new Atom("Platinum", "Pt", 78, 195.08);
        public static readonly Atom Plutonium = new Atom("Plutonium", "Pu", 94, 244.0642);
        public static readonly Atom Polonium = new Atom("Polonium", "Po", 84, 208.9824);
        public static readonly Atom Potassium = new Atom("Potassium", "K", 19, 39.0983);
        public static readonly Atom Praseodymium = new Atom("Praseodymium", "Pr", 59, 140.90765);
        public static readonly Atom Promethium = new Atom("Promethium", "Pm", 61, 144.9127);
        public static readonly Atom Protactinium = new Atom("Protactinium", "Pa", 91, 231.0388);
        public static readonly Atom Radium = new Atom("Radium", "Ra", 88, 226.0254);
        public static readonly Atom Radon = new Atom("Radon", "Rn", 86, 222.0176);
        public static readonly Atom Rhenium = new Atom("Rhenium", "Re", 75, 186.207);
        public static readonly Atom Rhodium = new Atom("Rhodium", "Rh", 45, 102.90550);
        public static readonly Atom Rubidium = new Atom("Rubidium", "Rb", 37, 85.4678);
        public static readonly Atom Ruthenium = new Atom("Ruthenium", "Ru", 44, 101.07);
        public static readonly Atom Samarium = new Atom("Samarium", "Sm", 62, 150.36);
        public static readonly Atom Scandium = new Atom("Scandium", "Sc", 21, 44.955910);
        public static readonly Atom Selenium = new Atom("Selenium", "Se", 34, 78.96);
        public static readonly Atom Silicon = new Atom("Silicon", "Si", 14, 28.0855);
        public static readonly Atom Silver = new Atom("Silver", "Ag", 47, 107.8682);
        public static readonly Atom Sodium = new Atom("Sodium", "Na", 11, 22.989768);
        public static readonly Atom Strontium = new Atom("Strontium", "Sr", 38, 87.62);
        public static readonly Atom Sulfur = new Atom("Sulfur", "S", 16, 32.066);
        public static readonly Atom Tantalum = new Atom("Tantalum", "Ta", 73, 180.9479);
        public static readonly Atom Technetium = new Atom("Technetium", "Tc", 43, 97.9072);
        public static readonly Atom Tellurium = new Atom("Tellurium", "Te", 52, 127.60);
        public static readonly Atom Terbium = new Atom("Terbium", "Tb", 65, 158.92534);
        public static readonly Atom Thallium = new Atom("Thallium", "Tl", 81, 204.3833);
        public static readonly Atom Thorium = new Atom("Thorium", "Th", 90, 232.0381);
        public static readonly Atom Thulium = new Atom("Thulium", "Tm", 69, 168.93421);
        public static readonly Atom Tin = new Atom("Tin", "Sn", 50, 118.710);
        public static readonly Atom Titanium = new Atom("Titanium", "Ti", 22, 47.867);
        public static readonly Atom Tungsten = new Atom("Tungsten", "W", 74, 183.84);
        public static readonly Atom Unnilquadium = new Atom("Unnilquadium", "Un", 104, 261.11);
        public static readonly Atom Unnilpentium = new Atom("Unnilpentium", "Un", 105, 262.114);
        public static readonly Atom Unnilhexium = new Atom("Unnilhexium", "Un", 106, 263.118);
        public static readonly Atom Unnilseptium = new Atom("Unnilseptium", "Un", 107, 262.12);
        public static readonly Atom Uranium = new Atom("Uranium", "U", 92, 238.0289);
        public static readonly Atom Vanadium = new Atom("Vanadium", "V", 23, 50.9415);
        public static readonly Atom Xenon = new Atom("Xenon", "Xe", 54, 131.29);
        public static readonly Atom Ytterbium = new Atom("Ytterbium", "Yb", 70, 173.04);
        public static readonly Atom Yttrium = new Atom("Yttrium", "Y", 39, 88.90585);
        public static readonly Atom Zinc = new Atom("Zinc", "Zn", 30, 65.39);
        public static readonly Atom Zirconium = new Atom("Zirconium", "Zr", 40, 91.224);
        #endregion
        public static readonly Atom[] All = new Atom[]
        {
            Actinium, Aluminum, Americium, Antimony, Argon, Arsenic, Astatine,
            Barium, Berkelium, Beryllium, Bismuth, Boron, Bromine, Cadmium, Calcium,
            Californium, Carbon, Cerium, Cesium, Chlorine, Chromium, Cobalt, Copper,
            Curium, Dysprosium, Einsteinium, Erbium, Europium, Fermium, Fluorine,
            Francium, Gadolinium, Gallium, Germanium, Gold, Hafnium, Helium, Holmium,
            Hydrogen, Indium, Iodine, Iridium, Iron, Krypton, Lanthanum, Lawrencium,
            Lead, Lithium, Lutetium, Magnesium, Manganese, Mendelevium, Mercury, Molybdenum,
            Neodymium, Neon, Neptunium, Nickel, Niobium, Nitrogen, Nobelium, Osmium,
            Oxygen, Palladium, Phosphorus, Platinum, Plutonium, Polonium, Potassium,
            Praseodymium, Promethium, Protactinium, Radium, Radon, Rhenium, Rhodium,
            Rubidium, Ruthenium, Samarium, Scandium, Selenium, Silicon, Silver, Sodium,
            Strontium, Sulfur, Tantalum, Technetium, Tellurium, Terbium, Thallium, Thorium,
            Thulium, Tin, Titanium, Tungsten, Unnilquadium, Unnilpentium, Unnilhexium,
            Unnilseptium, Uranium, Vanadium, Xenon, Ytterbium, Yttrium, Zinc, Zirconium
        };
        public string Name { get; private set; }
        public string Symbol { get; private set; }
        public int Number { get; private set; }
        public double Weigth { get; private set; }

        private Atom(string name, string symbol, int number, double weigth)
        {
            Name = name;
            Symbol = symbol;
            Number = number;
            Weigth = weigth;
        }

        public static ChemicalCompound operator *(Atom atom, int number)
        {
            return new ChemicalCompound(atom, number);
        }
        public static ChemicalCompound operator *(Atom l, Atom r)
        {
            return new ChemicalCompound(l, 1) * new ChemicalCompound(r, 1);
        }
    }
}
