﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScraperQA.Utility.Physics
{
    public class ChemicalCompound
    {
        private List<KeyValuePair<Atom, int>> atoms;

        public ChemicalCompound(IEnumerable<KeyValuePair<Atom, int>> atoms)
        {
            this.atoms = new List<KeyValuePair<Atom, int>>(atoms);
        }
        public ChemicalCompound(Atom atom, int number)
        {
            this.atoms = new List<KeyValuePair<Atom, int>>();
            this.atoms.Add(new KeyValuePair<Atom, int>(atom, number));
        }
        public ChemicalCompound(ChemicalCompound c1, ChemicalCompound c2)
        {
            this.atoms = new List<KeyValuePair<Atom, int>>();
            this.atoms.AddRange(c1.atoms);
            this.atoms.AddRange(c2.atoms);
        }

        public double MolecularWeight
        {
            get
            {
                return this.atoms.Select(k => k.Key.Weigth * k.Value).Sum();
            }
        }

        public static ChemicalCompound operator *(ChemicalCompound l, ChemicalCompound r)
        {
            return new ChemicalCompound(l, r);
        }
        public static ChemicalCompound operator *(Atom l, ChemicalCompound r)
        {
            return new ChemicalCompound(l, 1) * r;
        }
        public static ChemicalCompound operator *(ChemicalCompound l, Atom r)
        {
            return l * new ChemicalCompound(r, 1);
        }

        public override string ToString()
        {
            string s = "";
            this.atoms.ForEach(k => s += k.Key.Symbol + (k.Value == 1 ? "" : k.Value.ToString()));
            return s;
        }

        public static implicit operator ChemicalCompound(string s)
        {
            return Parse(s);
        }

        public static ChemicalCompound Parse(string s)
        {
            s = s.Replace(" ", "");
            int i = 0;
            List<KeyValuePair<Atom, int>> c = new List<KeyValuePair<Atom, int>>();
            while (i < s.Length)
            {
                Atom atom;
                i += (i + 1 < s.Length && IsAtomSymbol(s[i] + s[i + 1].ToString(), out atom)) ? 2 :
                    ((i < s.Length && IsAtomSymbol(s[i].ToString(), out atom)) ? 1 :
                    throw new ArgumentException("Invalid arguments"));
                int l = 0, n;
                while ((i + l) < s.Length && char.IsDigit(s[i + l])) l++;
                n = l > 0 ? int.Parse(s.Substring(i, l)) : 1;
                i += l;
                c.Add(new KeyValuePair<Atom, int>(atom, n));
            }
            return new ChemicalCompound(c);
        }
        private static bool IsAtomSymbol(string s, out Atom atom)
        {
            atom = Atom.All.Where(a => a.Symbol.Equals(s)).FirstOrDefault();
            return atom != null;
        }
    }
}
