﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ScraperQA.Utility.Files
{
    public class FileUtils
    {
        public static readonly Action<string> OnNewFileDefault = s => Utility.Logger.Log(String.Format("Reading file {0}", s), Utility.LogType.Warning, true);
        public static Action<IEnumerable<T>, int> GetOnNewRowDefault<T>()
        {
            return (r, i) => { if (i % 10000 == 0) { Utility.Logger.Log(String.Format("Read {0} rows", i), Utility.LogType.Info, false); } };
        }

        public static void IterateRows<T>(IEnumerable<string> paths, Func<string, IEnumerable<T>> converter, Action<string> onNewFile, Action<IEnumerable<T>, int> onNewRow)
        {
            foreach (string name in paths)
            {
                onNewFile.Invoke(name);
                int i = 0;
                try
                {
                    using (StreamReader sr = new StreamReader(name))
                    {
                        while (!sr.EndOfStream)
                        {
                            string line = sr.ReadLine();
                            IEnumerable<T> row = converter.Invoke(line);
                            onNewRow.Invoke(row, i);
                            i++;
                        }
                    }
                }
                catch (Exception e)
                {
                    Utility.Logger.Log("The file could not be parse:", Utility.LogType.Error);
                    Utility.Logger.Log(e.Message, Utility.LogType.Error);
                }
            }
        }

        public static IEnumerable<T> IterateRows<T>(IEnumerable<string> paths, Func<string, IEnumerable<T>> converter)
        {
            foreach (string name in paths)
            {
                StreamReader sr = null;
                try
                {
                    sr = new StreamReader(name);
                }
                catch (Exception e)
                {
                    Utility.Logger.Log("The file could not be open:", Utility.LogType.Error);
                    Utility.Logger.Log(e.Message, Utility.LogType.Error);
                    continue;
                }

                bool stop = true;

                try
                {
                    stop = sr.EndOfStream;
                }
                catch
                {
                    continue;
                }

                while (!stop)
                {
                    string line = null;
                    try
                    {
                        line = sr.ReadLine();
                    }
                    catch (Exception e)
                    {
                        Utility.Logger.Log("Error reading line.", Utility.LogType.Error);
                        Utility.Logger.Log(e.Message, Utility.LogType.Error);
                        break;
                    }

                    IEnumerable<T> row = converter(line);
                    foreach (T t in row)
                    {
                        yield return t;
                    }

                    try
                    {
                        stop = sr.EndOfStream;
                    }
                    catch
                    {
                        break;
                    }
                }
            }
        }

        public static void WriteCSV<T>(string path, string separator, IEnumerable<T> records) where T : ICSVWritable
        {
            File.Delete(path);
            ICSVWritable a = (ICSVWritable)Activator.CreateInstance(typeof(T));

            using (StreamWriter writer = new StreamWriter(path))
            {
                writer.WriteLine(String.Join(separator, a.GetCSVHeaders()));
                foreach (ICSVWritable r in records)
                {
                    writer.WriteLine(String.Join(separator, r.GetCSVValues()));
                }
            }
        }
    }
}
