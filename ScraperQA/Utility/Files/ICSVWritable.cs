﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScraperQA.Utility.Files
{
    public interface ICSVWritable
    {
        IEnumerable<string> GetCSVHeaders();
        IEnumerable<string> GetCSVValues();
    }
}
