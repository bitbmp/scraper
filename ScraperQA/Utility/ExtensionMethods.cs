﻿using System.Collections.Generic;

namespace ScraperQA
{
    public static class ExtensionMethods
    {
        public static T AddAndGet<T>(this List<T> set, T ins)
        {
            T res = ins;
            if (!set.Contains(ins))
            {
                set.Add(ins);
            }
            else
            {
                res = set.Find(rec => ins.Equals(rec));
            }
            return res;
        }

        public static IEnumerable<IEnumerable<T>> Batch<T>(
            this IEnumerable<T> source, int batchSize)
        {
            using (var enumerator = source.GetEnumerator())
                while (enumerator.MoveNext())
                    yield return YieldBatchElements(enumerator, batchSize - 1);
        }

        private static IEnumerable<T> YieldBatchElements<T>(
            IEnumerator<T> source, int batchSize)
        {
            yield return source.Current;
            for (int i = 0; i < batchSize && source.MoveNext(); i++)
                yield return source.Current;
        }
    }
}
