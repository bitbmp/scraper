﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScraperQA.Utility.Physics;

namespace ScraperQA.Utility.Unit
{
    public static class UnitConverter
    {
        //http://www.apis.ac.uk/unit-conversion
        public static double ppm_mgm3(double ppm, ChemicalCompound compound, double kelvin = 298.15, double hPa = 1013.25)
        {
            double molecularVolume = 22.41 * kelvin / 273.15 * 1013.25 / hPa;
            double a = 1 / molecularVolume;
            return ppm * compound.MolecularWeight / molecularVolume;
        }
        public static double ppb_ugm3(double ppb, ChemicalCompound compound, double kelvin = 298.15, double hPa = 1013.25)
        {
            return ppm_mgm3(ppb, compound, kelvin, hPa);
        }
        public static double ppm_ugm3(double ppb, ChemicalCompound compound, double kelvin = 298.15, double hPa = 1013.25)
        {
            return ppm_mgm3(ppb, compound, kelvin, hPa) * 1000.0;
        }
        public static double ppb_mgm3(double ppb, ChemicalCompound compound, double kelvin = 298.15, double hPa = 1013.25)
        {
            return ppm_mgm3(ppb, compound, kelvin, hPa) / 1000.0;
        }
    } 
}
