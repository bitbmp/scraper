﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ScraperQA.Utility.Visualization
{
    public class Marker
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("pos")]
        public MarkerPosition Position { get; set; }

        [JsonProperty("values")]
        public List<MarkerValue> Values { get; set; }
    }
    public class MarkerPosition
    {
        [JsonProperty("lat")]
        public double Latitude { get; set; }

        [JsonProperty("lng")]
        public double Longitude { get; set; }
    }
    public class MarkerValue
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("value")]
        public double Value { get; set; }

        [JsonProperty("unit")]
        public string Unit { get; set; }

        [JsonProperty("start")]
        public string Start { get; set; }

        [JsonProperty("end")]
        public string End { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }
}
