﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ScraperQA.Utility
{
    public class ConsoleCommand : Attribute
    {
        public string Description { get; set; }
        public ConsoleCommand(string description)
        {
            Description = description;
        }
    }
    public delegate void Print(string s);
    public static class CommandExecutor
    {
        public static void PrintHelp(object obj, Print printer)
        {
            MethodInfo[] methods = obj.GetType().GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance);
            methods = methods.Where(m => m.GetCustomAttribute<ConsoleCommand>() != null).ToArray();
            string[] cmd = methods.Select(m => m.Name.ToLower() + " - " + m.GetCustomAttribute<ConsoleCommand>().Description +
            (m.GetParameters().Length == 0 ? "" : "\n\t\t") +
            string.Join("\n\t\t", m.GetParameters().Select(p => p.Name + " : " + p.ParameterType.Name).ToArray())).ToArray();
            StringBuilder sb = new StringBuilder("\n");
            foreach (string c in cmd)
            {
                sb.Append("\t").Append(c).Append("\n");
            }
            printer("Commands list: " + sb.ToString());
        }
        public static bool Execute(object obj, string command, out object result)
        {
            result = null;
            
            List<string> parts = Regex.Matches(command, @"[\""].+?[\""]|[^ ]+")
                .Cast<Match>()
                .Select(m => m.Value.Trim('"'))
                .ToList();
            if (parts.Count == 0)
                return false;

            MethodInfo[] methods = obj.GetType().GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance);
            methods = methods.Where(m => m.Name.ToLower() == parts[0] && m.GetCustomAttribute<ConsoleCommand>() != null).ToArray();
            if (methods.Length == 0)
                return false;

            int paramsCount = parts.Count - 1;
            methods = methods.Where(m => m.GetParameters().Length == paramsCount).ToArray();

            foreach (MethodInfo mi in methods)
            {
                try
                {
                    ParameterInfo[] parameters = mi.GetParameters();
                    object[] parametersConverted = new object[parameters.Length];

                    for (int i = 0; i < paramsCount && i < parameters.Length; i++)
                    {
                        TypeConverter converter = TypeDescriptor.GetConverter(parameters[i].ParameterType);
                        parametersConverted[i] = converter.ConvertFrom(parts[i + 1]);
                    }

                    result = mi.Invoke(obj, parametersConverted);
                    return true;
                }
                catch
                {
                }
            }
            return false;
        }
    }
}
