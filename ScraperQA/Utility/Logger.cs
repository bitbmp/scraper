﻿using System;
using System.IO;

namespace ScraperQA.Utility
{
    public static class Logger
    {
        private const string filename = "log.txt";

        public static void Log(string message, LogType type = LogType.Info, bool logOnFile = true)
        {
            DateTime now = DateTime.Now;
            string fullMessage = "[" + type.ToString() + "] <" + now.ToString() + "> " + message;
            Console.ForegroundColor = type == LogType.Error ? ConsoleColor.Red : type == LogType.Warning ? ConsoleColor.Yellow : Console.ForegroundColor;
            Console.WriteLine(fullMessage);
            Console.ResetColor();
            if(logOnFile)
            {
                File.AppendAllText(filename, fullMessage + Environment.NewLine);
            }
        }

    }
    public enum LogType
    {
        Info,
        Warning,
        Error,
    }
}
