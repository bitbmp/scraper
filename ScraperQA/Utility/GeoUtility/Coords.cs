﻿namespace ScraperQA.Utility.GeoUtility
{
    public class Coords
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public Coords()
        {

        }
        public Coords(double lat, double lon)
        {
            Latitude = lat;
            Longitude = lon;
        }

        public double Distance(Coords other)
        {
            return GeoUtility.GetCoordsDistance(this.Latitude, this.Longitude, other.Latitude, other.Longitude);
        }
    }
}
