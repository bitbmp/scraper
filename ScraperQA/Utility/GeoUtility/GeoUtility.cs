﻿using System;

namespace ScraperQA.Utility.GeoUtility
{
    public static partial class GeoUtility
    {
        /// <summary>
        /// Return the distance in Km from 2 latitude and longitude
        /// </summary>
        public static double GetCoordsDistance(double lat1, double lon1, double lat2, double lon2)
        {
            double R = 6371; // Radius of the earth in km
            double dLat = DegToRad(lat2 - lat1);  // deg2rad below
            double dLon = DegToRad(lon2 - lon1);
            double a =
              Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
              Math.Cos(DegToRad(lat1)) * Math.Cos(DegToRad(lat2)) *
              Math.Sin(dLon / 2) * Math.Sin(dLon / 2)
              ;
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            var d = R * c; // Distance in km
            return d;
        }

        public static bool IsInsideCircle(double circle_center_lat, double circle_center_lon, double radius, double lat, double lon)
        {
            return GetCoordsDistance(circle_center_lat, circle_center_lon, lat, lon) <= radius;
        }

        public static double DegToRad(double deg)
        {
            return deg * (Math.PI / 180);
        }
    }
}
