﻿using Newtonsoft.Json;
using ScraperQA.Database;
using ScraperQA.Database.Creation;
using ScraperQA.Database.WebAPI;
using ScraperQA.OpenAQ;
using ScraperQA.Utility;
using ScraperQA.Utility.GeoUtility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScraperQA
{
    class ConsoleManager
    {
        public static void Start(string[] args)
        {
            Settings settings = JsonConvert.DeserializeObject<Settings>(File.ReadAllText("settings.json"));
            ConsoleManager consoleCommands = new ConsoleManager(settings);
            Logger.Log(" > Hello!", LogType.Info, true);
            CommandExecutor.PrintHelp(consoleCommands, s => Logger.Log(" > " + s, LogType.Info, false));

            while (consoleCommands.Active)
            {
                string cmd = Console.ReadLine().ToLower();
                Console.SetCursorPosition(0, Console.CursorTop - 1);
                Logger.Log(" < " + cmd, LogType.Info, true);

                object res;
                bool r = CommandExecutor.Execute(consoleCommands, cmd, out res);
                if (!r)
                {
                    Logger.Log(" > " + cmd + " command not found", LogType.Warning, true);
                }
            }
        }

        private Settings Settings { get; set; }
        public bool Active { get; set; }
        public ConsoleManager(Settings settings)
        {
            this.Settings = settings;
            this.Active = true;
        }

        private DatabaseManager GetDBManager()
        {
            return new DatabaseManager(String.Format("Host={0};Username={1};Password={2};Database={3};",
                Settings.Db.Host, Settings.Db.Username, Settings.Db.Password, Settings.Db.Database));
        }

        [ConsoleCommand("Print commands list")]
        public void Help()
        {
            CommandExecutor.PrintHelp(this, s => Logger.Log(" > " + s, LogType.Info, false));
        }
        [ConsoleCommand("exits")]
        public void Exit()
        {
            Active = false;
        }

        [ConsoleCommand("Create the db with current settings")]
        public void CreateDB()
        {
            DbCreator creator = new DbCreator(Settings.Db.Host, Settings.Db.Username, Settings.Db.Password, Settings.Db.Database);
            creator.Execute();
            Logger.Log(" > " + Settings.Db.Database + " db created", LogType.Info, true);
        }

        [ConsoleCommand("Runs the web server for data visualization and web API")]
        public void RunWebServer()
        {
            DbWebAPI webApi = new DbWebAPI(GetDBManager(), Settings.Db.DbWebAPI);
            Logger.Log(" > Web server started, press Enter to stop", LogType.Info, true);
            System.Diagnostics.Process.Start("http://" + Settings.Db.DbWebAPI.IPAddress + ":" + Settings.Db.DbWebAPI.Port +
                "/marker.html?q=date<='" + DateTime.Now.Add(TimeSpan.FromDays(1)).ToString("yyyy-MM-dd") + "' AND date>='" +
                DateTime.Now.ToString("yyyy-MM-dd") + "' AND value_type=" + ((int)Database.ValueType.Temperature));
            Console.ReadLine();
            webApi.Close();
            Logger.Log(" > " + "Web server stopped", LogType.Info, true);
        }

        [ConsoleCommand("Generate grid for machine learning. WORK IN PROGRESS")]
        public void GenerateGrid(string starttime, string endtime, double timeSlice, double latitude1, double longitude1, double latitude2, double longitude2, double spaceSlice)
        {
            Console.WriteLine(DateTime.ParseExact(starttime, "yyyy-MM-dd", CultureInfo.InvariantCulture));
            var db = GetDBManager();
            var startTime = DateTime.ParseExact(starttime, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            var endTime = DateTime.ParseExact(endtime, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            var result = db.Select<Record>(Record.DATE >= startTime & Record.DATE <= endTime)
                .Where(r => r.Latitude <= latitude1 && r.Latitude >= latitude2 &&
                            r.Longitude >= longitude1 && r.Longitude <= longitude2);

            int spaceXCount = (int)(GeoUtility.GetCoordsDistance(latitude1, longitude1, latitude1, longitude2) / spaceSlice + 1.5);
            int spaceYCount = (int)(GeoUtility.GetCoordsDistance(latitude1, longitude1, latitude2, longitude1) / spaceSlice + 1.5);
            int timeCount = (int)((endTime - startTime).TotalSeconds / timeSlice + 0.5);
            List<Record>[,,] records = new List<Record>[spaceXCount, spaceYCount, timeCount];
            Logger.Log(" > Matrix[x:" + spaceXCount + ", y:" + spaceYCount + ", t:" + timeCount + "]", LogType.Info, true);
            int count = 0;
            foreach (var r in result)
            {
                int spaceXIndex = (int)(GeoUtility.GetCoordsDistance(latitude1, r.Longitude, latitude1, longitude1) / spaceSlice + 0.5);
                int spaceYIndex = (int)(GeoUtility.GetCoordsDistance(r.Latitude, longitude1, latitude1, longitude1) / spaceSlice + 0.5);
                int timeIndex = (int)((endTime - r.Date).TotalSeconds / timeSlice + 0.5);

                records[spaceXIndex, spaceYIndex, timeIndex] = records[spaceXIndex, spaceYIndex, timeIndex] == null ? new List<Record>() : records[spaceXIndex, spaceYIndex, timeIndex];
                records[spaceXIndex, spaceYIndex, timeIndex].Add(r);

                count++;
            }
            Logger.Log(" > Generated matrix from " + count + " records", LogType.Info, true);
        }

        #region OpenAQ
        [ConsoleCommand("Imports data from OpenAQ")]
        public void ImportOpenAq()
        {
            ImportOpenAq(null);
        }
        [ConsoleCommand("Imports data from OpenAQ in a circle area. Radius in km")]
        public void ImportOpenAq(double latitude, double longitude, double radius)
        {
            ImportOpenAq(new QueryParams().Radius(radius * 1000.0).Coordinates(latitude, longitude));
        }
        [ConsoleCommand("Imports latest data from OpenAQ in a circle area. Radius in km")]
        public void ImportLatestOpenAq(double latitude, double longitude, double radius)
        {
            Logger.Log(" > Searching for data at " + latitude + "," + longitude + " within " + radius + "km", LogType.Info, true);
            var db = GetDBManager();
            var res = ResultSet<Latest>.GetResults(new QueryParams().Radius(radius * 1000.0).Coordinates(latitude, longitude));
            Logger.Log(" > Found " + res.Meta.Found + " records", LogType.Info, true);
            db.SmartInsert(res.Results.Select(l => l.ToRecord()).Aggregate((a, b) => new List<Record>(a.Concat(b))));
        }
        private void ImportOpenAq(QueryParams query)
        {
            if (query == null)
                query = new QueryParams();
            var db = GetDBManager();
            var meta = ResultSet<Measure>.GetResults(query.Clone().Limit(0)).Meta;
            Logger.Log(" > Found " + meta.Found + " records", LogType.Info, true);
            for (int page = 1; page <= (meta.Found / 10000 + 1); page++)
            {
                var res = ResultSet<Measure>.GetResults(query.Clone().Limit(10000).Page(page))
                    .Results.Select(m => m.ToRecord()).Where(r => r != null).Select(r => (Record)r);
                Logger.Log(" > Downloaded page " + page + " of " + (meta.Found / 10000 + 1) + " from OpenAQ", LogType.Info, true);
                db.SmartInsert(res);
            }
        }
        #endregion

        [ConsoleCommand("Imports latest n records from OpenAQ in a circle area. Radius in km, n <= 8400")]
        public void ImportPurpleAirData(int n, double latitude, double longitude, double radius)
        {
            var db = GetDBManager();
            db.SmartInsert(PurpleAirScraper.Scraper.ScrapeRecords(n, latitude, longitude, radius));
        }

    }
}
