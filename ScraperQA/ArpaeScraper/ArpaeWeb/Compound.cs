﻿using System.Collections.Generic;

namespace ScraperQA.ArpaeScraper.ArpaeWeb
{
    public class Compound
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Measurement> Measurements { get; set; } = new List<Measurement>();


        public Database.ValueType GetCompoundType()
        {
            switch(this.Id)
            {
                case 1:
                    return Database.ValueType.SO2;
                case 7:
                    return Database.ValueType.O3;
                case 111:
                    return Database.ValueType.PM25;
                case 10:
                    return Database.ValueType.CO;
                case 20:
                    return Database.ValueType.C6H6;
                case 8:
                    return Database.ValueType.NO2;
                case 5:
                    return Database.ValueType.PM10;
                default:
                    return (Database.ValueType)(-1);
            }
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Compound c = (Compound)obj;
            return this.Id == c.Id;
        }
    }
}
