﻿namespace ScraperQA.ArpaeScraper.ArpaeWeb
{
    public enum Province
    {
        BO,
        FC,
        PC,
        PR,
        RE,
        MO,
        FE,
        RA,
        RN,
    }
}
