﻿using ScraperQA.Database;
using System;

namespace ScraperQA.ArpaeScraper.ArpaeWeb
{
    public class Converters
    {
        public static readonly Func<Station, DbStation> StationToDbStation = s =>
        {
            return new DbStation(s.Id, s.Name) { Latitude = s.Coordinates.Latitude, Longitude = s.Coordinates.Longitude, Altitude = s.Coordinates.Altitude };
        };
    }
}
