﻿using System;
using System.Collections.Generic;

namespace ScraperQA.ArpaeScraper.ArpaeWeb
{
    public class Instrument
    {
        public string Name { get; set; }
        public List<Measurement> Measurements { get; set; } = new List<Measurement>();

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Instrument i = (Instrument)obj;
            return String.Equals(i.Name, this.Name);
        }
    }
}
