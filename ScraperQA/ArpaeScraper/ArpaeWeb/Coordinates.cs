﻿using System;

namespace ScraperQA.ArpaeScraper.ArpaeWeb
{
    public class Coordinates
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public double Altitude { get; set; }

        public override string ToString()
        {
            return String.Format("Alt: {0}, Lon: {1}, Lat: {2}", Altitude, Longitude, Latitude);
        }
    }
}
