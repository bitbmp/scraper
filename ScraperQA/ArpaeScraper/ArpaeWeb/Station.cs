﻿using HtmlAgilityPack;
using ScraperQA.Database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;

namespace ScraperQA.ArpaeScraper.ArpaeWeb
{
    public class Station
    {
        public string Name { get; set; }
        public int Id { get; set; }

        public string StationType { get; set; }
        public string ZoneType { get; set; }
        public string ZoneFeatures { get; set; }
        public DateTime InstallationDate { get; set; }
        public string Address { get; set; }
        public string Municipality { get; set; }
        public Province Province { get; set; }
        public Coordinates Coordinates { get; set; }
        public List<Compound> Compounds { get; set; }
        public List<Measurement> Measurements { get; set; } = new List<Measurement>();

        public static List<DbStation> GetAllDbStations()
        {
            return GetAllStations().Select(s => Converters.StationToDbStation(s)).ToList();
        }

        public static List<Station> GetAllStations()
        {
            List<Station> stations = new List<Station>();
            foreach (Province pv in Enum.GetValues(typeof(Province)))
            {
                stations.AddRange(GetStationByProvince(pv));
            }
            return stations;
        }

        public static List<Station> GetStationByProvince(Province province)
        {
            string url = "https://www.arpae.it/v2_rete_di_monitoraggio.asp?p=" + province;
            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(url);

            return doc.DocumentNode.SelectNodes("//select[@id='stazione']/option")
                .Select(n =>
                {
                    try
                    {
                        return Station.Parse(province, int.Parse(n.Attributes["value"].Value));
                    }
                    catch (Exception ex)
                    {
                        //Console.Write(ex.Message);
                        return null;
                    }
                }
                ).Where(it => it != null).ToList();
        }
        public static Station Parse(Province province, int id)
        {
            
            string url = "https://www.arpae.it/v2_rete_di_monitoraggio.asp?p=" + province + "&s=" + id;
            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(url);
            Station s = new Station();
            s.Id = id;
            s.Province = province;
            s.Name = doc.DocumentNode.SelectNodes("//select[@id='stazione']/option")
                .Where(n => n.Attributes["value"].Value.Equals(id.ToString()))
                .Select(n => n.InnerText)
                .FirstOrDefault();


            var html = doc.DocumentNode.SelectNodes("//div[@id='dettaglio']/p").First().InnerHtml;

            int i = 0, f;
            i = html.IndexOf("</b>", i) + 4;
            f = html.IndexOf("<br>", i);
            s.StationType = html.Substring(i, f - i).Trim();

            i = html.IndexOf("</b>", i) + 4;
            f = html.IndexOf("<br>", i);
            s.ZoneType = html.Substring(i, f - i).Trim();

            i = html.IndexOf("</b>", i) + 4;
            f = html.IndexOf("<br>", i);
            s.ZoneFeatures = html.Substring(i, f - i).Trim();

            i = html.IndexOf("</b>", i) + 4;
            f = html.IndexOf("<br>", i);
            s.InstallationDate = DateTime.ParseExact(html.Substring(i, f - i).Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);

            i = html.IndexOf("</b>", i) + 4;
            f = html.IndexOf("<br>", i);
            s.Address = html.Substring(i, f - i).Trim();

            i = html.IndexOf("</b>", i) + 4;
            f = html.IndexOf("<br>", i);
            s.Municipality = html.Substring(i, f - i).Trim();

            i = html.IndexOf("</b>", i) + 4;
            f = html.IndexOf("<br>", i);


            s.Coordinates = new Coordinates();
            i = html.IndexOf("</b>", i) + 4;
            f = html.IndexOf("<br>", i);
            s.Coordinates.Longitude = double.Parse(html.Substring(i, f - i).Trim().Replace(",", "."));
            i = html.IndexOf("</b>", i) + 4;
            f = html.IndexOf("<br>", i);
            s.Coordinates.Latitude = double.Parse(html.Substring(i, f - i).Trim().Replace(",", "."));
            i = html.IndexOf("</b>", i) + 4;
            f = html.IndexOf("<br>", i);
            s.Coordinates.Altitude = double.Parse(html.Substring(i, f - i).Trim().Trim('m'));


            s.Compounds = doc.DocumentNode.SelectNodes("//select[@id='q']/option")
                .Select(n =>
                {
                    Compound com = new Compound() { Id = int.Parse(n.Attributes["value"].Value), Name = n.InnerText };
                    com = GlobalInstances.Compounds.AddAndGet(com);
                    return com;
                }).ToList();
            return s;
        }

        private HtmlDocument GetRecordsHtmlDocument(Compound com, DateTime start, DateTime end)
        {
            string url = "https://www.arpae.it/estraistaz.asp?q=" + com.Id + "&i=" + start.ToString("dd.MM.yyyy") + "&f=" + end.ToString("dd.MM.yyyy") + "&s=" + this.Id + "&p=" + this.Province;
            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(url);
            return doc;
        }

        public DataTable GetStationRecords(Compound com, DateTime start, DateTime end)
        {
            if (!this.Compounds.Contains(com))
            {
                return null;
            }

            HtmlDocument doc = GetRecordsHtmlDocument(com, start, end);
            DataTable table = new DataTable();

            doc.DocumentNode.Descendants("thead").ToList().ForEach(re => re.Descendants("th").Select(r => WebUtility.HtmlDecode(r.InnerText)).ToList().ForEach(r => table.Columns.Add(r)));
            doc.DocumentNode.Descendants("tbody").ToList().ForEach(re => re.Descendants("tr").ToList().ForEach(row =>
            {
                var values = row.Descendants("td").Select(r => WebUtility.HtmlDecode(r.InnerText)).ToList();
                table.Rows.Add(values.ToArray());
            }));

            return table;

        }


        private DateTime ParseDateTime(string s)
        {
            DateTime res;
            if (!DateTime.TryParseExact(s, "dd/MM/yyyy H.mm.ss", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out res))
            {
                if (!DateTime.TryParseExact(s, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out res))
                {
                    throw new Exception("Invalid start date.");
                }
            }
            return res;
        }

        public void UpdateMeasurements(DateTime start, DateTime end)
        {
            foreach (Compound com in Compounds)
            {
                HtmlDocument doc = GetRecordsHtmlDocument(com, start, end);

                doc.DocumentNode.Descendants("tbody").ToList().ForEach(re => re.Descendants("tr").ToList().ForEach(row =>
                {
                    var values = row.Descendants("td").Select(r => WebUtility.HtmlDecode(r.InnerText)).ToList();
                    DateTime st;
                    if (!DateTime.TryParseExact(values[1], "dd/MM/yyyy H.mm.ss", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out st))
                    {
                        if (!DateTime.TryParseExact(values[1], "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out st))
                        {
                            throw new Exception("Invalid start date.");
                        }
                    }
                    DateTime en;
                    if (!DateTime.TryParseExact(values[2], "dd/MM/yyyy H.mm.ss", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out en))
                    {
                        if (!DateTime.TryParseExact(values[2], "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out en))
                        {
                            throw new Exception("Invalid start date.");
                        }
                    }

                    Period per = new Period() { startDate = st, endDate = en };
                    per = GlobalInstances.Periods.AddAndGet(per);
                    Instrument ins = new Instrument() { Name = values[3] };
                    ins = GlobalInstances.Instruments.AddAndGet(ins);
                    string unit = values[5];

                    double val;
                    if (Double.TryParse(values[4], out val))
                    {
                        Measurement m = new Measurement()
                        {
                            SourceStation = this,
                            Compound = com,
                            Period = per,
                            Instrument = ins,
                            Value = (unit == "mg/m3" ? val * 1000 : val),
                            Unit = "ug/m3",
                            F1 = !String.IsNullOrWhiteSpace(values[6]) && values[6] == "1",
                            F2 = !String.IsNullOrWhiteSpace(values[7]) && values[7] == "1",
                            F3 = !String.IsNullOrWhiteSpace(values[8]) && values[8] == "1",
                            F4 = !String.IsNullOrWhiteSpace(values[9]) && values[9] == "1"
                        };
                        com.Measurements.Add(m);
                        ins.Measurements.Add(m);
                        per.Measurements.Add(m);
                        this.Measurements.Add(m);
                    }
                }));
            }
        }



        public List<Record> GetRecords(DateTime start, DateTime end)
        {
            List<Record> res = new List<Record>();
            foreach (Compound com in Compounds)
            {
                HtmlDocument doc = GetRecordsHtmlDocument(com, start, end);
                foreach (HtmlNode row in doc.DocumentNode.Descendants("tbody").SelectMany(r => r.Descendants("tr")))
                {
                    string[] rowValues = row.Descendants("td").Select(r => WebUtility.HtmlDecode(r.InnerText)).ToArray();
                    Record? rec = GetRecordFromRow(this, com, rowValues);
                    if (rec != null)
                    {
                        res.Add((Record)rec);
                    }
                }

            }
            return res;
            /*
            return Compounds.SelectMany(com =>
            GetRecordsHtmlDocument(com, start, end).DocumentNode.Descendants("tbody").SelectMany(re =>
            re.Descendants("tr")).Select(row => GetRecordFromRow(this, com, row.Descendants("td").Select(r =>
            WebUtility.HtmlDecode(r.InnerText)).ToArray()))).Where(r => r.Valid).ToList();*/
        }

        private Record? GetRecordFromRow(Station s, Compound c, string[] values)
        {
            Record res = new Record();
            double val;
            if (Double.TryParse(values[4], out val))
            {
                DateTime st = ParseDateTime(values[1]);
                DateTime en = ParseDateTime(values[2]);

                res.Altitude = s.Coordinates.Altitude;
                res.Latitude = s.Coordinates.Latitude;
                res.Longitude = s.Coordinates.Longitude;
                res.Source = s.Id;
                res.ValueType = (int)c.GetCompoundType();
                res.Value = (values[5] == "mg/m3" ? val * 1000 : val);
                res.Date = st;
                res.AggregationType = (int)AggregationType.Average;
                res.Span = en - st;
                return res;
            }
            else
            {
                return null;
            }
        }
    }
}
