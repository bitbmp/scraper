﻿using ScraperQA.Database;
using System;

namespace ScraperQA.ArpaeScraper.ArpaeWeb
{
    public class Measurement
    {
        public Period Period { get; set; }
        public Station SourceStation { get; set; }
        public Instrument Instrument { get; set; }
        public Compound Compound { get; set; }
        public double Value { get; set; }
        public string Unit { get; set; }
        public bool F1 { get; set; }
        public bool F2 { get; set; }
        public bool F3 { get; set; }
        public bool F4 { get; set; }

        public Record ConvertToRecord()
        {
            Record res = new Record();
            res.Altitude = this.SourceStation.Coordinates.Altitude;
            res.Latitude = this.SourceStation.Coordinates.Latitude;
            res.Longitude = this.SourceStation.Coordinates.Longitude;
            res.Source = this.SourceStation.Id;
            res.ValueType = (int)this.Compound.GetCompoundType();
            res.Value = this.Value;
            res.Date = new DateTime((this.Period.startDate.Ticks + this.Period.endDate.Ticks) / 2);
            return res;
        }
    }
}
