﻿using System.Collections.Generic;

namespace ScraperQA.ArpaeScraper.ArpaeWeb
{
    public static class GlobalInstances
    {
        public static List<Period> Periods { get; set; } = new List<Period>();
        public static List<Instrument> Instruments { get; set; } = new List<Instrument>();
        public static List<Compound> Compounds { get; set; } = new List<Compound>();
    }
}
