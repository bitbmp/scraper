﻿using System;
using System.Collections.Generic;

namespace ScraperQA.ArpaeScraper.ArpaeWeb
{
    public class Period
    {
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public List<Measurement> Measurements { get; set; } = new List<Measurement>();

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Period p = (Period)obj;
            return DateTime.Equals(this.startDate, p.startDate) && DateTime.Equals(this.endDate, p.endDate);
        }
    }
}
