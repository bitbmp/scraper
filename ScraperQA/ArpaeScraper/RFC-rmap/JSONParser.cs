﻿using ScraperQA.Database;
using ScraperQA.Utility;
using ScraperQA.Utility.Files;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ScraperQA.ArpaeScraper.RFC_rmap
{
    public static class JSONParser
    {
        public static List<T> FilesParse<T>(IEnumerable<string> paths, Func<string, IEnumerable<T>> converter, Func<T, bool> filter)
        {
            return FilesParse<T>(paths, converter, filter, FileUtils.OnNewFileDefault, FileUtils.GetOnNewRowDefault<T>());
        }

        public static List<T> FilesParse<T>(IEnumerable<string> paths, Func<string, IEnumerable<T>> converter, Func<T, bool> filter, Action<string> onNewFile, Action<IEnumerable<T>, int> onNewRow)
        {
            List<T> res = new List<T>();
            Action<IEnumerable<T>, int> act = (r, i) =>
            {
                foreach (T v in r)
                {
                    if (filter.Invoke(v))
                    {
                        res.Add(v);
                    }

                }
            };
            act += onNewRow;
            FileUtils.IterateRows<T>(paths, converter, onNewFile, act);
            return res;
        }

        public static List<Record> FilesParse(IEnumerable<string> paths)
        {
            return FilesParse<Record>(paths, Converters.StringToRecordListConverter, r => true);
        }

        public static List<Record> FileParse(string path)
        {
            return FilesParse<Record>(new string[] { path }, Converters.StringToRecordListConverter, r => true);
        }



        public static void GenerateAttributesStats(IEnumerable<string> paths)
        {
            List<BLocalType> types = new List<BLocalType>();
            List<BLocalType> stationsTypes = new List<BLocalType>();
            List<TimeRangeType> timeRanges = new List<TimeRangeType>();
            List<LevelType> levelTypesL1 = new List<LevelType>();
            List<LevelType> levelTypesL2 = new List<LevelType>();
            List<long> P1s = new List<long>();
            List<long> P2s = new List<long>();
            List<double?> L1s = new List<double?>();
            List<double?> L2s = new List<double?>();
            

            Action<IEnumerable<RmapLocation>, int> act = (r, i) =>
            {
                foreach (RmapLocation row in r)
                {
                    foreach (KeyValuePair<BLocalType, object> entry in row.StationInfos)
                    {
                        stationsTypes.AddAndGet(entry.Key);
                    }

                    foreach (RmapValues vals in row.Data)
                    {
                        P1s.AddAndGet(vals.P1);
                        P2s.AddAndGet(vals.P2);
                        L1s.AddAndGet(vals.L1);
                        L2s.AddAndGet(vals.L2);
                        levelTypesL1.AddAndGet(vals.L1Type);
                        levelTypesL2.AddAndGet(vals.L2Type);
                        timeRanges.AddAndGet(vals.TimeType);
                        foreach (KeyValuePair<BLocalType, object> entry in vals.Values)
                        {
                            types.AddAndGet(entry.Key);
                        }
                    }
                }
            };

            FileUtils.IterateRows<RmapLocation>(paths, Converters.StringToRmapLocationConverter, FileUtils.OnNewFileDefault, act);

            File.WriteAllLines("types.txt", types.Select(r => r.ToString()));
            File.WriteAllLines("stationsTypes.txt", stationsTypes.Select(r => r.ToString()));
            File.WriteAllLines("timeRanges.txt", timeRanges.Select(r => r.ToString()));
            File.WriteAllLines("levelTypesL1.txt", levelTypesL1.Select(r => r.ToString()));
            File.WriteAllLines("levelTypesL2.txt", levelTypesL2.Select(r => r.ToString()));
            File.WriteAllLines("P1s.txt", P1s.Select(r => r.ToString()));
            File.WriteAllLines("P2s.txt", P2s.Select(r => r.ToString()));
            File.WriteAllLines("L1s.txt", L1s.Select(r => r.ToString()));
            File.WriteAllLines("L2s.txt", L2s.Select(r => r.ToString()));
        }

        public static HashSet<DbStation> dbStations(IEnumerable<string> paths)
        {
            HashSet<DbStation> hs = new HashSet<DbStation>();
            Action<IEnumerable<DbStation>, int> act = (line, i) =>
            {
                foreach(DbStation s in line)
                {
                    hs.Add(s);
                }
                FileUtils.GetOnNewRowDefault<DbStation>().Invoke(line, i);
            };

            FileUtils.IterateRows<DbStation>(paths, Converters.StringToDbStation, FileUtils.OnNewFileDefault, act);
            return hs;
        }
    }
}
