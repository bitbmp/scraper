﻿using System;
using System.Collections.Generic;

namespace ScraperQA.ArpaeScraper.RFC_rmap
{
    public class RmapValues
    {
        public TimeRangeType TimeType { get; set; }
        public long P1 { get; set; }
        public long P2 { get; set; }
        public Dictionary<BLocalType, object> Values { get; set; } = new Dictionary<BLocalType, object>();
        public LevelType L1Type { get; set; }
        public long? L1 { get; set; }
        public LevelType L2Type { get; set; }
        public long? L2 { get; set; }

        public override string ToString()
        {
            return String.Format("RawRmapValues[TimeType: {0}, P1: {1}, P2: {2}, L1Type: {3}, L1: {4}, L2Type: {5}, L2: {6}]",
                TimeType, P1, P2, L1Type, L1, L2Type, L2);
        }

    }
}
