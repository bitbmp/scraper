﻿using Newtonsoft.Json;

namespace ScraperQA.ArpaeScraper.RFC_rmap
{
    public class RmapSettings
    {
        [JsonProperty("JSONPath")]
        public string JSONPath { get; set; }
    }
}
