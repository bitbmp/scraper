﻿using System;
using System.Collections.Generic;

namespace ScraperQA.ArpaeScraper.RFC_rmap
{
    public class RmapLocation
    {
        public string Ident { get; set; }
        public string Network { get; set; }
        public long Lon { get; set; }
        public DateTime Date { get; set; }
        public string Version { get; set; }
        public long Lat { get; set; }
        public Dictionary<BLocalType, object> StationInfos { get; set; } = new Dictionary<BLocalType, object>();
        public List<RmapValues> Data { get; set; } = new List<RmapValues>();

        public override string ToString()
        {
            return String.Format("RmapLocation[Ident: {0}, Network: {1}, Lon: {2}, Lat: {3}, Date: {4}, Version: {5}, StationInfos: {6}]",
                Ident, Network, Lon, Lat, Date, Version, StationInfos);
        }
    }
}
