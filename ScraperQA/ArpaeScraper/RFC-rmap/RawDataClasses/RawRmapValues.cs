﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ScraperQA.ArpaeScraper.RFC_rmap.RawDataClasses
{
    public class RawRmapValues
    {
        [JsonProperty("timerange", NullValueHandling = NullValueHandling.Ignore)]
        public List<long> Timerange { get; set; }

        [JsonProperty("vars", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, RawRmapSingleValue> Vars { get; set; }

        [JsonProperty("level", NullValueHandling = NullValueHandling.Ignore)]
        public List<long?> Level { get; set; }

        public override string ToString()
        {
            return String.Format("RawRmapValues[Timerange: {0}, Vars: {1}, Level: {2}]",
                Timerange.ToString(), Vars.ToString(), Level.ToString());
        }
    }
}
