﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ScraperQA.ArpaeScraper.RFC_rmap.RawDataClasses
{
    public class RawRmapLocation
    {
        [JsonProperty("ident")]
        public string Ident { get; set; }

        [JsonProperty("network")]
        public string Network { get; set; }

        [JsonProperty("lon")]
        public long Lon { get; set; }

        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }

        [JsonProperty("lat")]
        public long Lat { get; set; }

        [JsonProperty("data")]
        public List<RawRmapValues> Data { get; set; }

        

        public RmapLocation GetRmapLocation()
        {
            return Converters.ToRmapLocationConverter.Invoke(this).FirstOrDefault();
        }

        public override string ToString()
        {
            return String.Format("RawRmapLocation[Ident: {0}, Network: {1}, Lon: {2}, Lat: {3}, Date: {4}, Version: {5}]",
                Ident, Network, Lon, Lat, Date, Version);
        }
    }
}
