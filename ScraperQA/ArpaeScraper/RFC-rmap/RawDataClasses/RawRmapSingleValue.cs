﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ScraperQA.ArpaeScraper.RFC_rmap.RawDataClasses
{
    public class RawRmapSingleValue
    {
        [JsonProperty("a", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, string> A { get; set; }

        [JsonProperty("v")]
        public string V { get; set; }

        public override string ToString()
        {
            return String.Format("RawRmapSingleValue[A: {0}, V: {1}]",
                A, V);
        }
    }
}
