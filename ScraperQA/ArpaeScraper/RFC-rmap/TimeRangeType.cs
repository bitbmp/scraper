﻿namespace ScraperQA.ArpaeScraper.RFC_rmap
{
    public enum TimeRangeType
    {
        Average = 0,
        Accumulation = 1,
        Maximum = 2,
        Minimum = 3,
        Difference = 4,
        RootMeanSquare = 5,
        StandardDeviation = 6,
        Covariance = 7,
        Difference2 = 8,
        Ratio = 9,
        ClimatologicalMeanValue = 51,
        VectorialMean = 200,
        Mode = 201,
        StandardDeviationVectorialMean = 202,
        VectorialMaximum = 203,
        VectorialMinimum = 204,
        ProductWithValidTimeRangingInsideThGivenPeriod = 205,
        Istantaneousvalue = 254,
    }
}
