﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ScraperQA.ArpaeScraper.RFC_rmap.RawDataClasses;
using ScraperQA.Database;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace ScraperQA.ArpaeScraper.RFC_rmap
{
    public class Converters
    {
        internal class Converter
        {
            public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
            {
                MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
                DateParseHandling = DateParseHandling.None,
                Converters = {
                    new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AdjustToUniversal }
                },
            };
        }

        public static readonly Func<string, IEnumerable<RawRmapLocation>> ToRawRmapLocationConverter = line =>
        {
            RawRmapLocation rawrow = JsonConvert.DeserializeObject<RawRmapLocation>(line, Converter.Settings);
            return new List<RawRmapLocation> { rawrow };
        };

        public static readonly Func<string, IEnumerable<RmapLocation>> StringToRmapLocationConverter = line =>
        {
            List<RmapLocation> res = new List<RmapLocation>();
            IEnumerable<RawRmapLocation> rawrows = ToRawRmapLocationConverter.Invoke(line);
            foreach(RawRmapLocation val in rawrows)
            {
                res.AddRange(ToRmapLocationConverter(val));
            }
            return res;
        };

        public static readonly Func<RmapLocation, IEnumerable<DbStation>> RmapLocationToDbStation = loc =>
        {
            object o_name = null;
            loc.StationInfos.TryGetValue(new BLocalType(1019, "", "", "", null), out o_name);
            string name = o_name != null ? Convert.ToString(o_name) : "";

            object o_lat = null;
            if(!loc.StationInfos.TryGetValue(new BLocalType(5001, "", "", "", null), out o_lat))
            {
                return Enumerable.Empty<DbStation>();
            }
            double lat = Convert.ToDouble(o_lat);

            object o_lon = null;
            if (!loc.StationInfos.TryGetValue(new BLocalType(6001, "", "", "", null), out o_lon))
            {
                return Enumerable.Empty<DbStation>();
            }
            double lon = Convert.ToDouble(o_lon);

            object o_alt = null;
            loc.StationInfos.TryGetValue(new BLocalType(7030, "", "", "", null), out o_alt);
            double alt = o_alt != null ? Convert.ToDouble(o_alt) : 0;

            DbStation stat = new DbStation((name+lat.ToString()+lon.ToString()).GetHashCode(), name) { Longitude = lon, Latitude = lat, Altitude = alt };
            return new List<DbStation>() { stat };

        };

        public static readonly Func<string, IEnumerable<DbStation>> StringToDbStation = line =>
        {
            List<DbStation> res = new List<DbStation>();
            IEnumerable<RawRmapLocation> rawrows = ToRawRmapLocationConverter.Invoke(line);
            foreach (RawRmapLocation val in rawrows)
            {
                IEnumerable<RmapLocation> l = ToRmapLocationConverter(val);
                foreach (RmapLocation val2 in l)
                {
                    res.AddRange(RmapLocationToDbStation.Invoke(val2));
                }
            }
            return res;
        };

        public static readonly Func<RawRmapLocation, IEnumerable<RmapLocation>> ToRmapLocationConverter = raw =>
        {
            RmapLocation res = new RmapLocation();
            List<RmapValues> d = new List<RmapValues>();
            res.Ident = raw.Ident;
            res.Network = raw.Network;
            res.Lon = raw.Lon;
            res.Date = raw.Date;
            res.Version = raw.Version;
            res.Lat = raw.Lat;

            foreach (RawRmapValues rv in raw.Data)
            {
                RmapValues v = new RmapValues();
                Dictionary<BLocalType, object> values = new Dictionary<BLocalType, object>();

                foreach (KeyValuePair<string, RawRmapSingleValue> kv in rv.Vars)
                {
                    /*Value is valid, if A is defined the value is invalid*/
                    if (kv.Value.A == null || kv.Value.A.Count == 0)
                    {
                        int type_code = int.Parse(kv.Key.Replace('B', '0'));
                        BLocalType type = BLocalType.GetBLocalType(type_code);
                        object value = Convert.ChangeType(kv.Value.V, type.Type);
                        values.Add(type, value);
                    }
                }

                if (rv.Timerange != null && rv.Level != null)
                {
                    v.Values = values;
                    v.TimeType = (TimeRangeType)rv.Timerange[0];
                    v.P1 = rv.Timerange[1];
                    v.P2 = rv.Timerange[2];
                    v.L1Type = LevelType.GetLevelType(Convert.ToInt32(rv.Level[0]));
                    v.L1 = rv.Level[1];
                    v.L2Type = LevelType.GetLevelType(Convert.ToInt32(rv.Level[2]));
                    v.L2 = rv.Level[3];
                    res.Data.Add(v);
                }
                else
                {
                    /*this record contains station info*/
                    res.StationInfos = values;
                }
            }
            return new List<RmapLocation>() { res };
        };

        public static readonly Func<RawRmapLocation, IEnumerable<Record>> ToRecordListConverter = raw =>
        {
            List<Record> res = new List<Record>();
            RmapLocation row = ToRmapLocationConverter.Invoke(raw).First();

            foreach(RmapValues v in row.Data)
            {
                foreach(KeyValuePair<BLocalType, object> pair in v.Values)
                {
                    Record r = new Record();
                    r.Date = row.Date;
                    r.Span = new TimeSpan(0, 0, Convert.ToInt32(v.P2));
                    r.Latitude = row.Lat / 100000.0;
                    r.Longitude = row.Lon / 100000.0;

                    Database.ValueType? vt = ToValueType(pair.Key);
                    if (vt == null)
                    {
                        continue;
                    }
                    r.ValueType = (int)vt;

                    r.Value = Convert.ToDouble(pair.Value);

                    AggregationType? at = ToAggregationType(v.TimeType);
                    if (at == null)
                    {
                        continue;
                    }
                    r.AggregationType = (int)at;

                    
                    //7031: height of barometer, 7030: height of station
                    int blcode = 7030;
                    if (vt == Database.ValueType.Pressure)
                    {
                        blcode = 7031;
                        r.Value /= 100; //convert to hectopascal
                    }

                    object o_stationAltitude = null;
                    row.StationInfos.TryGetValue(new BLocalType(blcode, "", "", "", null), out o_stationAltitude);
                    double stationAltitude = o_stationAltitude != null ? Convert.ToDouble(o_stationAltitude) : 0;

                    switch (v.L1Type.Code)
                    {
                        case 1:
                            r.Altitude = 0;
                            break;
                        case 103:
                            r.Altitude = (Convert.ToInt32(v.L1) / 1000.0);
                            break;
                        case 106:
                            r.Altitude = -(Convert.ToInt32(v.L1) / 1000.0);
                            break;
                        default:
                            r.Altitude = 0;
                            break;
                    }

                    r.Altitude += stationAltitude;

                    object o_stationName = null;
                    row.StationInfos.TryGetValue(new BLocalType(1019, "", "", "", null), out o_stationName);
                    string stationName = o_stationName != null ? Convert.ToString(o_stationName) : "";

                    object o_lat = null;
                    row.StationInfos.TryGetValue(new BLocalType(5001, "", "", "", null), out o_lat);
                    double lat = o_lat != null ? Convert.ToDouble(o_lat) : -1;

                    object o_lon = null;
                    row.StationInfos.TryGetValue(new BLocalType(6001, "", "", "", null), out o_lon);
                    double lon = o_lon != null ? Convert.ToDouble(o_lon) : -1;

                    r.Source = (stationName + lat.ToString() + lon.ToString()).GetHashCode();

                    res.Add(r);
                    
                }
            }

            return res;

        };

        public static readonly Func<string, IEnumerable<Record>> StringToRecordListConverter = line =>
        {
            List<Record> res = new List<Record>();
            IEnumerable<RawRmapLocation> raw = ToRawRmapLocationConverter.Invoke(line);
            foreach (RawRmapLocation val in raw)
            {
                res.AddRange(ToRecordListConverter.Invoke(val));
            }
            return res;
        };

            public static Database.ValueType? ToValueType(BLocalType blt)
        {
            switch (blt.Code)
            {
                case 13011:
                    return Database.ValueType.Precipitation;
                case 12101:
                    return Database.ValueType.Temperature;
                case 13003:
                    return Database.ValueType.Humidity;
                case 11002:
                    return Database.ValueType.WindSpeed;
                case 11001:
                    return Database.ValueType.WindDirection;
                case 10004:
                    return Database.ValueType.Pressure;
                case 14198:
                    return Database.ValueType.VisibleRadiation;
                case 14021:
                    return Database.ValueType.GlobalSolarRadiation;
                default:
                    return null;
            }
        }

        public static AggregationType? ToAggregationType(TimeRangeType trt)
        {
            switch (trt)
            {
                case TimeRangeType.Istantaneousvalue:
                    return AggregationType.Instantaneous;
                case TimeRangeType.VectorialMean:
                    return AggregationType.Instantaneous; //not an error
                case TimeRangeType.Accumulation:
                    return AggregationType.Accumulation;
                case TimeRangeType.Maximum:
                    return AggregationType.Maximum;
                case TimeRangeType.Minimum:
                    return AggregationType.Minimum;
                case TimeRangeType.Average:
                    return AggregationType.Average;
                default:
                    return null;
            }
        }
    }
}
