﻿using System.Collections.Generic;

namespace ScraperQA.ArpaeScraper.RFC_rmap
{
    public static class Filters
    {
        public static readonly List<int> types = new List<int>
        {
            13011,
            12101,
            13003,
            11002,
            11001,
            10004,
            14198,
            14021
        };

        public static readonly List<int> aggregations = new List<int>
        {
            0, //average
            1, //accumulation
            2, //maximum
            3, //minimum
            200, //vectorial
            254 //istantaneous
        };

        public static readonly List<int> levels = new List<int>
        {
            1,
            103,
            106
        };

    }
}
