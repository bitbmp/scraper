﻿using System;
using System.Collections.Generic;

namespace ScraperQA.ArpaeScraper.RFC_rmap
{
    public class LevelType
    {
        public int Code { get; set; }
        public string Name { get; set; }
        public string Unit { get; set; }

        public LevelType(int code, string name, string unit)
        {
            this.Code = code;
            this.Name = name;
            this.Unit = unit;
        }
        public override string ToString()
        {
            return String.Format("{0}: {1} | {2}", Code, Name, Unit);
        }


        private static Dictionary<int, LevelType> values = new Dictionary<int, LevelType>();
        static LevelType()
        {
            values.Add(0, new LevelType(0, "Reserved", ""));
            values.Add(1, new LevelType(1, "Ground or Water Surface", ""));
            values.Add(2, new LevelType(2, "Cloud Base Level", ""));
            values.Add(3, new LevelType(3, "Level of Cloud Tops", ""));
            values.Add(4, new LevelType(4, "Level of 0C Isotherm", ""));
            values.Add(5, new LevelType(5, "Level of Adiabatic Condensation Lifted from the Surface", ""));
            values.Add(6, new LevelType(6, "Maximum Wind Level", ""));
            values.Add(7, new LevelType(7, "Tropopause", ""));
            values.Add(8, new LevelType(8, "Nominal Top of the Atmosphere", "DB-All.e encodes the channel number of polar satellites in L1"));
            values.Add(9, new LevelType(9, "Sea Bottom", ""));
            //values.Add(10 - 19, new LevelType(10 - 19, "Reserved", ""));
            values.Add(20, new LevelType(20, "Isothermal Level", "K/10"));
            //values.Add(21 - 99, new LevelType(21 - 99, "Reserved", ""));
            values.Add(100, new LevelType(100, "Isobaric Surface", "Pa"));
            values.Add(101, new LevelType(101, "Mean Sea Level", ""));
            values.Add(102, new LevelType(102, "Specific Altitude Above Mean Sea Level", "mm"));
            values.Add(103, new LevelType(103, "Specified Height Level Above Ground", "mm"));
            values.Add(104, new LevelType(104, "Sigma Level", "1/10000"));
            values.Add(105, new LevelType(105, "Hybrid Level", ""));
            values.Add(106, new LevelType(106, "Depth Below Land Surface", "mm"));
            values.Add(107, new LevelType(107, "Isentropic (theta) Level", "K/10"));
            values.Add(108, new LevelType(108, "Level at Specified Pressure Difference from Ground to Level", "Pa"));
            values.Add(109, new LevelType(109, "Potential Vorticity Surface", "10-9 K m2 kg-1 s-1"));
            values.Add(110, new LevelType(110, "Reserved", ""));
            values.Add(111, new LevelType(111, "Eta (NAM) Level (see note below)", "1/10000"));
            values.Add(112, new LevelType(112, "116 Reserved", ""));
            values.Add(117, new LevelType(117, "Mixed Layer Depth", "mm"));
            values.Add(160, new LevelType(160, "Depth Below Sea Level", "mm"));
            values.Add(200, new LevelType(200, "Entire atmosphere (considered as a single layer)", ""));
            values.Add(201, new LevelType(201, "Entire ocean (considered as a single layer)", ""));
            values.Add(204, new LevelType(204, "Highest tropospheric freezing level", ""));
            values.Add(206, new LevelType(206, "Grid scale cloud bottom level", ""));
            values.Add(207, new LevelType(207, "Grid scale cloud top level", ""));
            values.Add(209, new LevelType(209, "Boundary layer cloud bottom level", ""));
            values.Add(210, new LevelType(210, "Boundary layer cloud top level", ""));
            values.Add(211, new LevelType(211, "Boundary layer cloud layer", ""));
            values.Add(212, new LevelType(212, "Low cloud bottom level", ""));
            values.Add(213, new LevelType(213, "Low cloud top level", ""));
            values.Add(214, new LevelType(214, "Low cloud layer", ""));
            values.Add(215, new LevelType(215, "Cloud ceiling", ""));
            values.Add(220, new LevelType(220, "Planetary Boundary Layer", ""));
            values.Add(222, new LevelType(222, "Middle cloud bottom level", ""));
            values.Add(223, new LevelType(223, "Middle cloud top level", ""));
            values.Add(224, new LevelType(224, "Middle cloud layer", ""));
            values.Add(232, new LevelType(232, "High cloud bottom level", ""));
            values.Add(233, new LevelType(233, "High cloud top level", ""));
            values.Add(234, new LevelType(234, "High cloud layer", ""));
            values.Add(235, new LevelType(235, "Ocean Isotherm Level", "K/10"));
            values.Add(240, new LevelType(240, "Ocean Mixed Layer", ""));
            values.Add(241, new LevelType(241, "Ordered Sequence of Data", ""));
            values.Add(242, new LevelType(242, "Convective cloud bottom level", ""));
            values.Add(243, new LevelType(243, "Convective cloud top level", ""));
            values.Add(244, new LevelType(244, "Convective cloud layer", ""));
            values.Add(245, new LevelType(245, "Lowest level of the wet bulb zero", ""));
            values.Add(246, new LevelType(246, "Maximum equivalent potential temperature level", ""));
            values.Add(247, new LevelType(247, "Equilibrium level", ""));
            values.Add(248, new LevelType(248, "Shallow convective cloud bottom level", ""));
            values.Add(249, new LevelType(249, "Shallow convective cloud top level", ""));
            values.Add(251, new LevelType(251, "Deep convective cloud bottom level", ""));
            values.Add(252, new LevelType(252, "Deep convective cloud top level", ""));
            values.Add(253, new LevelType(253, "Lowest bottom level of supercooled liquid water layer", ""));
            values.Add(254, new LevelType(254, "Highest top level of supercooled liquid water layer", ""));
            values.Add(256, new LevelType(256, "Clouds", ""));
            values.Add(257, new LevelType(257, "Information about the station that generated the data", ""));
            values.Add(258, new LevelType(258, "(use when ltype1=256) Cloud Data group, L2 = 1 low clouds, 2 middle clouds, 3 high clouds, 0 others", ""));
            values.Add(259, new LevelType(259, "(use when ltype1=256) Individual cloud groups, L2 = group number", ""));
            values.Add(260, new LevelType(260, "(use when ltype1=256) Cloud drift, L2 = group number", ""));
            values.Add(261, new LevelType(261, "(use when ltype1=256) Cloud elevation, L2 = group number; (use when ltype1=264) L2 = swell wave group number", ""));
            values.Add(262, new LevelType(262, "(use when ltype1=256) Direction and elevation of clouds, L2 is ignored", ""));
            values.Add(263, new LevelType(263, "(use when ltype1=256) Cloud groups with bases below station level, L2 = group number", ""));
            values.Add(264, new LevelType(264, "Waves", ""));
            values.Add(265, new LevelType(265, "Non-physical data level", "engineering ordinal level"));
        }
        public static LevelType GetLevelType(int code)
        {
            if (values.ContainsKey(code))
            {
                return new LevelType(code, values[code].Name, values[code].Unit);
            }
            return null;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            LevelType p = (LevelType)obj;
            return this.Code == p.Code &&
                this.Name == p.Name &&
                this.Unit == p.Unit;
        }
    }
}
