﻿using ScraperQA.ArpaeScraper.ArpaeWeb;
using ScraperQA.Database;
using ScraperQA.Database.Conditions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace ScraperQA.ArpaeScraper.DailyClimateAnalysis
{
    public class Extractor
    {
        private Dictionary<int, Coordinates> cells = new Dictionary<int, Coordinates>();
        private string tminFile, tmaxFile, precFile;

        public Extractor(string cellFile, string tminFile, string tmaxFile, string precFile)
        {
            this.tminFile = tminFile;
            this.tmaxFile = tmaxFile;
            this.precFile = precFile;

            StreamReader reader = new StreamReader(cellFile);
            reader.ReadLine();
            while (!reader.EndOfStream)
            {
                string l = reader.ReadLine();
                List<string> columns = l.Split('"').Select((element, index) => index % 2 == 0 // If even index 
                ? element.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries) // Split the item 
                : new string[] { element }) // Keep the entire item 
                .SelectMany(element => element).ToList();

                int id = int.Parse(columns[0]);

                Coordinates coords = new Coordinates()
                {
                    Altitude = double.Parse(columns[5].Replace(",", "."), CultureInfo.InvariantCulture),
                    Longitude = double.Parse(columns[6].Replace(",", "."), CultureInfo.InvariantCulture),
                    Latitude = double.Parse(columns[7].Replace(",", "."), CultureInfo.InvariantCulture),
                };
                cells.Add(id, coords);
            }
            reader.Close();
        }

        public IEnumerable<Record> Read(Condition condition)
        {
            return Condition.Filter(GetDailyRecord(tminFile, Database.ValueType.Temperature, AggregationType.Minimum)
                .Concat(GetDailyRecord(tmaxFile, Database.ValueType.Temperature, AggregationType.Maximum))
                .Concat(GetDailyRecord(precFile, Database.ValueType.Precipitation, AggregationType.Accumulation))
                ,condition);
        }

        public IEnumerable<Record> GetDailyRecord(string file, Database.ValueType ct, AggregationType at)
        {
            StreamReader reader = new StreamReader(file);
            while (!reader.EndOfStream)
            {
                string l = reader.ReadLine().Trim();
                int c = int.Parse(l.Substring(0, 5));
                int y = int.Parse(l.Substring(5, 4));
                int m = int.Parse(l.Substring(9, 2));
                int d = int.Parse(l.Substring(11, 2));
                double v = double.Parse(l.Substring(13, 6).Trim());

                DateTime date = new DateTime(y, m, d);
                Coordinates coords = cells[c];
                yield return new Record()
                {
                    Source = c,
                    Date = new DateTime(y, m, d),
                    Span = TimeSpan.FromDays(1),
                    Latitude = coords.Latitude,
                    Longitude = coords.Longitude,
                    Altitude = coords.Altitude,
                    Value = v,
                    ValueType = (int)ct,
                    AggregationType = (int)at,
                };
            }
            reader.Close();
        }
    }
}
