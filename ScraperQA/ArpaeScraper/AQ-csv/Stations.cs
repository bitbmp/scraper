﻿using ScraperQA.Database;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ScraperQA.ArpaeScraper.AQ_csv
{
    public static class Stations
    {
        public static readonly List<Station> stations = Station.FromJson(File.ReadAllText(@"ArpaeScraper\AQ-csv\Stations.json"));

        public static List<DbStation> GetDbStations()
        {
            return stations.Select(s => Converters.StationToDbStation(s)).ToList();
        }
    }
}
