﻿namespace ScraperQA.ArpaeScraper.AQ_csv
{
    public enum Parameters
    {
        SO2 = 1,
        PM10 = 5,
        O3 = 7,
        NO2 = 8,
        NOX = 9,
        CO = 10,
        C6H6 = 20,
        C6H5CH3 = 21,
        NO = 38,
        PM25 = 111,
        oxylene = 82
    }
}
