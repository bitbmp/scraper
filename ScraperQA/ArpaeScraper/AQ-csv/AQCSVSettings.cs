﻿using Newtonsoft.Json;

namespace ScraperQA.ArpaeScraper.AQ_csv
{
    public class AQCSVSettings
    {
        [JsonProperty("CSVPath")]
        public string CSVPath { get; set; }
    }
}
