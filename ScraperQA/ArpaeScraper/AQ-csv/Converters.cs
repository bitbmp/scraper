﻿using ScraperQA.Database;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace ScraperQA.ArpaeScraper.AQ_csv
{
    public class Converters
    {
        public static readonly Func<Station, DbStation> StationToDbStation = s =>
        {
            return new DbStation((int)s.CodStaz, s.Stazione) { Latitude = s.Lat, Longitude = s.Lon, Altitude = s.Altezza };
        };
        public static readonly Func<string, IEnumerable<Record>> ToRecordConverter = line =>
        {
            List<Record> res = new List<Record>();
            string[] values = line.Split(',');
            int station_code = 0;
            int id_param = 0;
            DateTime start = default(DateTime);
            DateTime end = default(DateTime);
            double value = 0;
            bool ok = int.TryParse(values[0], out station_code) &&
                        int.TryParse(values[1], out id_param) &&
                        DateTime.TryParseExact(values[2], "dd/MM/yyyy HH", CultureInfo.GetCultureInfo("it-IT"), DateTimeStyles.AssumeLocal, out start) &&
                        DateTime.TryParseExact(values[3], "dd/MM/yyyy HH", CultureInfo.GetCultureInfo("it-IT"), DateTimeStyles.AssumeLocal, out end) &&
                        Double.TryParse(values[4], out value);
            if(start.Month == 6)
            {
                Console.Write("");
            }
            if (ok)
            {
                Station station = Stations.stations.Find(s => s.CodStaz == station_code);
                if(station != null)
                {
                    TimeSpan span = end - start;
                    Record r = new Record();
                    r.AggregationType = (int)AggregationType.Average;
                    r.Altitude = station.Altezza;
                    r.Date = start.ToUniversalTime();
                    r.Latitude = station.Lat;
                    r.Longitude = station.Lon;
                    r.Span = span;
                    r.Value = value;
                    r.Source = station_code;
                    Database.ValueType? vt = ToValueType((Parameters)id_param);
                    if(vt != null)
                    {
                        r.ValueType = (int)vt;
                        res.Add(r);
                    }
                }
            }
            return res;
        };

        public static Database.ValueType? ToValueType(Parameters p)
        {
            switch(p)
            {
                case Parameters.C6H5CH3:
                    return Database.ValueType.C6H5CH3;
                case Parameters.C6H6:
                    return Database.ValueType.C6H6;
                case Parameters.CO:
                    return Database.ValueType.CO;
                case Parameters.NO:
                    return Database.ValueType.NO;
                case Parameters.NO2:
                    return Database.ValueType.NO2;
                case Parameters.NOX:
                    return Database.ValueType.NOX;
                case Parameters.O3:
                    return Database.ValueType.O3;
                case Parameters.oxylene:
                    return Database.ValueType.OXYLENE;
                case Parameters.PM10:
                    return Database.ValueType.PM10;
                case Parameters.PM25:
                    return Database.ValueType.PM25;
                case Parameters.SO2:
                    return Database.ValueType.SO2;
                default:
                    return null;
            }
        }
    }
}
