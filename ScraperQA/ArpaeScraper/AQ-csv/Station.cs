﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.Globalization;

namespace ScraperQA.ArpaeScraper.AQ_csv
{
    public partial class Station
    {
        [JsonProperty("Stazione")]
        public string Stazione { get; set; }

        [JsonProperty("Cod_staz")]
        public long CodStaz { get; set; }

        [JsonProperty("COMUNE")]
        public string Comune { get; set; }

        [JsonProperty("INDIRIZZO")]
        public string Indirizzo { get; set; }

        [JsonProperty("PROVINCIA")]
        public string Provincia { get; set; }

        [JsonProperty("Altezza")]
        public long Altezza { get; set; }

        [JsonProperty("Coord_X")]
        public double? CoordX { get; set; }

        [JsonProperty("Coord_Y")]
        public double? CoordY { get; set; }

        [JsonProperty("SR")]
        public long? Sr { get; set; }

        [JsonProperty("Lon")]
        public double Lon { get; set; }

        [JsonProperty("Lat")]
        public double Lat { get; set; }

        [JsonProperty("SR_GEO")]
        public long SrGeo { get; set; }
    }

    public partial class Station
    {
        public static List<Station> FromJson(string json) => JsonConvert.DeserializeObject<List<Station>>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this List<Station> self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
