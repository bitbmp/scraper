﻿using Newtonsoft.Json;
using ScraperQA.OpenWeatherMap.Model;
using System;
using System.Globalization;
using System.Net;

namespace ScraperQA.OpenWeatherMap
{
    public class OwmClient
    {
        private OwmSettings settings;
        public OwmClient(OwmSettings settings)
        {
            this.settings = settings;
        }

        public OwmCurrentData GetCurrentDataByCoords(double latitude, double longitude)
        {
            string url = String.Format("{0}{1}?lat={2}&lon={3}&appid={4}",
                settings.BaseUrl, settings.CurrentWeatherUrl, latitude.ToString(CultureInfo.InvariantCulture), longitude.ToString(CultureInfo.InvariantCulture), settings.ApiKey);
            string data = GetData(url);
            OwmCurrentData record = JsonConvert.DeserializeObject<OwmCurrentData>(data);
            return record;
        }
        public OwmCurrentData GetCurrentDataByCityId(long cityId)
        {
            string url = String.Format("{0}{1}?id={2}&appid={3}",
                settings.BaseUrl, settings.CurrentWeatherUrl, cityId, settings.ApiKey);
            string data = GetData(url);
            OwmCurrentData record = JsonConvert.DeserializeObject<OwmCurrentData>(data);
            return record;
        }
        public OwmCurrentData GetCurrentDataByCityId(OwmCity city)
        {
            return GetCurrentDataByCityId(city.Id);
        }

        private string GetData(string url)
        {
            return new WebClient().DownloadString(url);
        }
    }
}
