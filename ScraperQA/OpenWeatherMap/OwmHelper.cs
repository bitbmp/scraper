﻿using Newtonsoft.Json;
using ScraperQA.Database;
using ScraperQA.OpenWeatherMap.Model;
using ScraperQA.Utility.GeoUtility;
using System;
using System.Collections.Generic;
using System.IO;

namespace ScraperQA.OpenWeatherMap
{
    public class OwmHelper
    {
        public static List<Record> OwmCurrentDataToRecord(OwmCurrentData data)
        {
            List<Record> records = new List<Record>();

            if (data.Main != null)
            {
                if (data.Main.Temperature != null)
                {
                    Record r = GetBaseRecord(data, (double)data.Main.Temperature, Database.ValueType.Temperature, AggregationType.Instantaneous);
                    records.Add(r);
                }
                if (data.Main.Humidity != null)
                {
                    Record r = GetBaseRecord(data, (double)data.Main.Humidity, Database.ValueType.Humidity, AggregationType.Instantaneous);
                    records.Add(r);
                }
                if (data.Main.TemperatureMax != null)
                {
                    Record r = GetBaseRecord(data, (double)data.Main.TemperatureMax, Database.ValueType.Temperature, AggregationType.Maximum);
                    records.Add(r);
                }
                if (data.Main.TemperatureMin != null)
                {
                    Record r = GetBaseRecord(data, (double)data.Main.TemperatureMin, Database.ValueType.Temperature, AggregationType.Minimum);
                    records.Add(r);
                }

                //PRESSURE
                if (data.Main.Pressure != null && (data.Main.SeaLevel == null && data.Main.SeaLevel == null)) //on sea level
                {
                    Record r = GetBaseRecord(data, (double)data.Main.Pressure, Database.ValueType.Pressure, AggregationType.Instantaneous, 0);
                    records.Add(r);
                }
                if (data.Main.SeaLevel != null)
                {
                    Record r = GetBaseRecord(data, (double)data.Main.SeaLevel, Database.ValueType.Pressure, AggregationType.Instantaneous, 0);
                    records.Add(r);
                }
                if (data.Main.GrndLevel != null) // ??
                {
                    Record r = GetBaseRecord(data, (double)data.Main.GrndLevel, Database.ValueType.Pressure, AggregationType.Instantaneous, 1);
                    records.Add(r);
                }
            }
            if (data.Wind != null)
            {
                if (data.Wind.Speed != null)
                {
                    Record r = GetBaseRecord(data, (double)data.Wind.Speed, Database.ValueType.WindSpeed, AggregationType.Instantaneous);
                    records.Add(r);
                }
                if (data.Wind.Degrees != null)
                {
                    Record r = GetBaseRecord(data, (double)data.Wind.Degrees, Database.ValueType.WindDirection, AggregationType.Instantaneous);
                    records.Add(r);
                }
            }
            if (data.Clouds != null && data.Clouds.All != null)
            {
                Record r = GetBaseRecord(data, (double)data.Clouds.All, Database.ValueType.Cloudiness, AggregationType.Instantaneous);
                records.Add(r);
            }
            if (data.Rain != null && data.Rain.Volume3h != null)
            {
                Record r = GetBaseRecord(data, (double)data.Rain.Volume3h, Database.ValueType.Precipitation, AggregationType.Accumulation);
                r.Span = TimeSpan.FromHours(3);
                records.Add(r);
            }
            if (data.Snow != null && data.Snow.Volume3h != null)
            {
                Record r = GetBaseRecord(data, (double)data.Snow.Volume3h, Database.ValueType.SnowPrecipitation, AggregationType.Accumulation);
                r.Span = TimeSpan.FromHours(3);
                records.Add(r);
            }

            return records;
        }

        private static Record GetBaseRecord(OwmCurrentData data, double value, Database.ValueType vt, AggregationType at, double? altitude = null)
        {
            return new Record()
            {
                Date = (DateTime)data.RecordTime,
                Span = TimeSpan.FromSeconds(0), //??

                Latitude = data.Coordinates.Latitude,
                Longitude = data.Coordinates.Longitude,
                Altitude = altitude == null ? 0 : (double)altitude, //??

                Value = value,
                ValueType = (int)vt,
                AggregationType = (int)at,

                Source = (int?)data.CityId,
            };
        }

        public static List<OwmCity> GetAllCityFromFile(string file = "city.list.min.json")
        {
            return JsonConvert.DeserializeObject<List<OwmCity>>(File.ReadAllText(file));
        }
        public static IEnumerable<OwmCity> GetCityInCircleArea(IEnumerable<OwmCity> cities, double center_latitude, double center_longitude, double radius_km)
        {
            foreach(var c in cities)
            {
                if(GeoUtility.GetCoordsDistance(c.Coordinates.Latitude, c.Coordinates.Longitude, 
                    center_latitude, center_longitude) < radius_km)
                {
                    yield return c;
                }
            }
        }
    }
}
