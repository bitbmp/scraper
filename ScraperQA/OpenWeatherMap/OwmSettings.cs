﻿using Newtonsoft.Json;

namespace ScraperQA.OpenWeatherMap
{
    public class OwmSettings
    {
        [JsonProperty("api_key")]
        public string ApiKey { get; set; }

        [JsonProperty("base_url")]
        public string BaseUrl { get; set; }

        [JsonProperty("current_weather_url")]
        public string CurrentWeatherUrl { get; set; }

    }
}
