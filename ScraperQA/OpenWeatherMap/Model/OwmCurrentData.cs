﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ScraperQA.OpenWeatherMap.Model
{
    public class OwmCurrentData
    {
        /// <summary>
        /// Geo location
        /// </summary>
        [JsonProperty("coord")]
        public OwmCoord Coordinates { get; set; }

        /// <summary>
        /// List of weather conditions
        /// </summary>
        [JsonProperty("weather")]
        public List<OwmWeather> Weather { get; set; }

        /// <summary>
        /// Internal parameter
        /// </summary>
        [JsonProperty("base")]
        public string Base { get; set; }

        /// <summary>
        /// Main data
        /// </summary>
        [JsonProperty("main")]
        public OwmMain Main { get; set; }

        /// <summary>
        /// Wind data
        /// </summary>
        [JsonProperty("wind")]
        public OwmWind Wind { get; set; }

        /// <summary>
        /// Clouds data
        /// </summary>
        [JsonProperty("clouds")]
        public OwmCloud Clouds { get; set; }

        /// <summary>
        /// Rain data
        /// </summary>
        [JsonProperty("rain")]
        public OwmRain Rain { get; set; }

        /// <summary>
        /// Snow data
        /// </summary>
        [JsonProperty("snow")]
        public OwmSnow Snow { get; set; }

        /// <summary>
        /// Time of data calculation, unix, UTC
        /// </summary>
        [JsonProperty("dt")]
        public long? RecordTimeSeconds { get; set; }

        /// <summary>
        /// Time of data calculation
        /// </summary>
        [JsonIgnore]
        public DateTime? RecordTime
        {
            get
            {
                if (RecordTimeSeconds == null) return null;
                return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds((long)RecordTimeSeconds).ToLocalTime();
            }
        }

        /// <summary>
        /// System data
        /// </summary>
        [JsonProperty("sys")]
        public OwmSystem System { get; set; }

        /// <summary>
        /// City ID
        /// </summary>
        [JsonProperty("id")]
        public long? CityId { get; set; }

        /// <summary>
        /// City name
        /// </summary>
        [JsonProperty("name")]
        public string CityName { get; set; }

        /// <summary>
        /// Internal parameter
        /// </summary>
        [JsonProperty("cod")]
        public string Cod { get; set; }
    }
}
