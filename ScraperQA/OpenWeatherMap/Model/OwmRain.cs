﻿using Newtonsoft.Json;

namespace ScraperQA.OpenWeatherMap.Model
{
    public class OwmRain
    {
        /// <summary>
        /// Rain volume for the last 3 hours
        /// </summary>
        [JsonProperty("3h")]
        public double? Volume3h { get; set; }
    }
}
