﻿using Newtonsoft.Json;

namespace ScraperQA.OpenWeatherMap.Model
{
    public class OwmCloud
    {
        /// <summary>
        /// Cloudiness, %
        /// </summary>
        [JsonProperty("all")]
        public double? All { get; set; }
    }
}
