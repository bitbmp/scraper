﻿using Newtonsoft.Json;
using System;

namespace ScraperQA.OpenWeatherMap.Model
{
    public class OwmSystem
    {
        /// <summary>
        /// Internal parameter
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// Internal parameter
        /// </summary>
        [JsonProperty("id")]
        public long? Id { get; set; }

        /// <summary>
        /// Internal parameter
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }

        /// <summary>
        /// Country code (GB, JP etc.)
        /// </summary>
        [JsonProperty("country")]
        public string Country { get; set; }

        /// <summary>
        /// Sunrise time, unix, UTC
        /// </summary>
        [JsonProperty("sunrise")]
        public long? SunriseSeconds { get; set; }

        /// <summary>
        /// Sunset time, unix, UTC
        /// </summary>
        [JsonProperty("sunset")]
        public long? SunsetSeconds { get; set; }

        /// <summary>
        /// Time of data calculation
        /// </summary>
        [JsonIgnore]
        public DateTime? Sunrise
        {
            get
            {
                if (SunriseSeconds == null) return null;
                return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds((long)SunriseSeconds).ToLocalTime();
            }
        }

        /// <summary>
        /// Time of data calculation
        /// </summary>
        [JsonIgnore]
        public DateTime? Sunset
        {
            get
            {
                if (SunsetSeconds == null) return null;
                return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds((long)SunsetSeconds).ToLocalTime();
            }
        }
    }
}
