﻿using Newtonsoft.Json;
using ScraperQA.Database;
using ScraperQA.Database.Conditions;
using ScraperQA.OpenWeatherMap;
using ScraperQA.OpenWeatherMap.Model;
using ScraperQA.Utility;
using ScraperQA.Utility.GeoUtility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace ScraperQA
{
    public class OwmUpdater
    {
        private static Condition condition =
            (!(((DbColumnName)"ValueType") == (int)Database.ValueType.Temperature &
               ((DbColumnName)"AggregationType") == (int)AggregationType.Maximum)) &

            (!(((DbColumnName)"ValueType") == (int)Database.ValueType.Temperature
             & ((DbColumnName)"AggregationType") == (int)AggregationType.Minimum));

        public List<OwmCity> Cities { get; set; }
        public Coords[] Grid { get; set; }
        private OwmClient client;
        private DatabaseManager db;
        private int callsPerMinute;
        private bool run;
        private Thread th;

        public OwmUpdater(OwmClient client, DatabaseManager db, int callsPerMinute = 55)
        {
            this.db = db;
            this.client = client;
            this.callsPerMinute = callsPerMinute;
            this.run = false;
            this.th = null;
        }

        public void Start(UpdateType type)
        {
            if (th != null)
            {
                Logger.Log("OwmERUpdater => " + type + " can't start.", LogType.Warning, true);
                return;
            }
            Logger.Log("OwmERUpdater => " + type + " started.", LogType.Info, true);

            if (type == UpdateType.City)
            {
                run = true;
                th = new Thread(UpdateCity);
                th.Start();
            }
            if (type == UpdateType.Grid)
            {
                run = true;
                th = new Thread(UpdateGrid);
                th.Start();
            }
        }
        public void Stop()
        {
            run = false;
            th.Join();
        }

        public enum UpdateType
        {
            City,
            Grid,
        }


        private void UpdateCity()
        {
            if (Cities == null || Cities.Count == 0)
            {
                Logger.Log("OwmERUpdater.UpdateCity no city found", LogType.Warning, true);
                return;
            }
            db.Insert<DbStation>(Cities.Select(c => new DbStation((int)c.Id, c.Name)
            { Latitude = c.Coordinates.Latitude, Longitude=c.Coordinates.Longitude, Altitude = 0 }));

            TimeSpan span = TimeSpan.FromSeconds(60.0 / callsPerMinute);
            int idx = 0;
            while (run)
            {
                DateTime start = DateTime.Now;
                try
                {
                    OwmCurrentData data = client.GetCurrentDataByCityId(Cities[idx]);
                    List<Record> records = OwmHelper.OwmCurrentDataToRecord(data);
                    Logger.Log("Inserting " + Cities[idx].Name + " data [" + (idx + 1) + "/" + Cities.Count + "]", LogType.Info, false);
                    db.Insert(Condition.Filter(records, condition));
                }
                catch (Exception ex)
                {
                    Logger.Log("OwmERUpdater.UpdateCity exception: " + ex.Message, LogType.Error, true);
                }
                idx = (idx + 1) % Cities.Count;

                TimeSpan sleepTime = span.Subtract(DateTime.Now - start);
                if (sleepTime.Ticks > 0)
                {
                    Thread.Sleep(sleepTime);
                }
            }

            Logger.Log("OwmERUpdater.UpdateCity stopped", LogType.Info, true);
        }

        private void UpdateGrid()
        {
            TimeSpan span = TimeSpan.FromSeconds(60.0 / callsPerMinute);
            int idx = 0;
            while (run)
            {
                DateTime start = DateTime.Now;
                try
                {
                    OwmCurrentData data = client.GetCurrentDataByCoords(Grid[idx].Latitude, Grid[idx].Longitude);
                    List<Record> records = OwmHelper.OwmCurrentDataToRecord(data);

                    Logger.Log("Inserting [" + Grid[idx].Latitude + ", " + Grid[idx].Longitude + "] data [" + (idx + 1) + "/" + Grid.Length + "]", LogType.Info, false);
                    db.Insert(Condition.Filter(records, condition));
                }
                catch (Exception ex)
                {
                    Logger.Log("OwmERUpdater.UpdateGrid exception: " + ex.Message, LogType.Error, true);
                }
                idx = (idx + 1) % Grid.Length;

                TimeSpan sleepTime = span.Subtract(DateTime.Now - start);
                if (sleepTime.Ticks > 0)
                {
                    Thread.Sleep(sleepTime);
                }
            }

            Logger.Log("OwmERUpdater.UpdateGrid stopped", LogType.Info, true);
        }
    }
}
