﻿using ScraperQA.Database;
using ScraperQA.Utility;
using ScraperQA.Utility.GeoUtility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ScraperQA.PurpleAirScraper
{
    public static class Scraper
    {
        public static IEnumerable<Record> ScrapeAllRecords()
        {
            List<Station> sts = Stations.GetAllStations().ToList();
            int i = 1;
            foreach (Station s in sts)
            {
                Logger.Log(String.Format("Reading station [{1}/{2}] {0}", s.ToString(), i, sts.Count), LogType.Warning);
                IEnumerable<Record> recs = s.GetLastNRecords();
                foreach (Record r in recs)
                {
                    yield return r;
                }
                i++;
            }
        }
        public static IEnumerable<Record> ScrapeRecords(int n, double centerLat, double centerLon, double km)
        {
            List<Station> sts = Stations.GetAllStations()
                .Where(s => GeoUtility.GetCoordsDistance(centerLat, centerLon, s.Latitude, s.Longitude) < km).ToList();
            int i = 1;
            foreach (Station s in sts)
            {
                Logger.Log(String.Format("Reading station [{1}/{2}] {0}", s.ToString(), i, sts.Count), LogType.Warning);
                IEnumerable<Record> recs = s.GetLastNRecords(n);
                foreach (Record r in recs)
                {
                    yield return r;
                }
                i++;
            }
        }
    }
}
