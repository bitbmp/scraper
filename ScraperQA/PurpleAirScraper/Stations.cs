﻿using ScraperQA.PurpleAirScraper.RawDataClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;

namespace ScraperQA.PurpleAirScraper
{
    public static class Stations
    {
        //public static readonly List<Station> stations = GetAllStations.Invoke();
        private static List<Station> stations = null;

        public static Func<ReadOnlyCollection<Station>> GetAllStations = () => {
            if (stations == null)
            {
                stations = new List<Station>();
                using (WebClient wc = new WebClient())
                {
                    string json = wc.DownloadString("https://www.purpleair.com/json");
                    RawMap map = RawMap.FromJson(json);
                    stations.AddRange(Converters.RawStationsToStations.Invoke(map.Stations));
                }
            }
            return stations.AsReadOnly();
        };
    }
}
