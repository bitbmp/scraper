﻿using ScraperQA.Database;
using ScraperQA.PurpleAirScraper.RawDataClasses;
using ScraperQA.Utility;
using System;
using System.Collections.Generic;
using System.Net;

namespace ScraperQA.PurpleAirScraper
{
    public class Station : DbStation
    {
        public Station Twin { get; set; }

        public bool IsInside { get; set; }
        public string Hardware { get; set; }

        public int TSId { get; set; }
        public string TSKey { get; set; }


        public Station() 
            : base()
        {

        }

        public Station(int id, Station twin, string name, double lat, double lon,
            double alt, bool isInside, string hardware, int tsid, string tskey) 
            : base (id, name)
        {
            Twin = twin;
            Latitude = lat;
            Longitude = lon;
            Altitude = alt;
            IsInside = isInside;
            Hardware = hardware;
            TSId = tsid;
            TSKey = tskey;
        }

        public IEnumerable<Record> GetLastNRecords(int n = 8400)
        {
            using (WebClient wc = new WebClient())
            {
                string json = null;
                try
                {
                    json = wc.DownloadString(String.Format("https://api.thingspeak.com/channels/{0}/feed.json?results={1}&api_key={2}", TSId, n, TSKey));
                } catch(Exception ex)
                {
                    Logger.Log(String.Format("Failed fetching {0} : {1}", this, ex.Message), LogType.Error, true);
                    yield break;
                }
                
                RawRecords rs = RawRecords.FromJson(json);
                foreach(Record r in Converters.RawRecordsToDbRecords(rs, this))
                {
                    yield return r;
                }
            }
        }
    }
}
