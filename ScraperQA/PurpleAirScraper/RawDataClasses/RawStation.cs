﻿using Newtonsoft.Json;

namespace ScraperQA.PurpleAirScraper.RawDataClasses
{
    public class RawStation
    {
        [JsonProperty("ID")]
        public long Id { get; set; }

        [JsonProperty("ParentID")]
        public long? ParentId { get; set; }

        [JsonProperty("Label")]
        public string Label { get; set; }

        [JsonProperty("DEVICE_LOCATIONTYPE")]
        public string DeviceLocationtype { get; set; }

        [JsonProperty("THINGSPEAK_PRIMARY_ID")]
        public string ThingspeakPrimaryId { get; set; }

        [JsonProperty("THINGSPEAK_PRIMARY_ID_READ_KEY")]
        public string ThingspeakPrimaryIdReadKey { get; set; }

        [JsonProperty("THINGSPEAK_SECONDARY_ID")]
        public string ThingspeakSecondaryId { get; set; }

        [JsonProperty("THINGSPEAK_SECONDARY_ID_READ_KEY")]
        public string ThingspeakSecondaryIdReadKey { get; set; }

        [JsonProperty("Lat")]
        public double? Lat { get; set; }

        [JsonProperty("Lon")]
        public double? Lon { get; set; }

        [JsonProperty("PM2_5Value")]
        public string Pm25Value { get; set; }

        [JsonProperty("LastSeen")]
        public long LastSeen { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("Hidden")]
        public string Hidden { get; set; }

        [JsonProperty("Flag")]
        public long? Flag { get; set; }

        [JsonProperty("isOwner")]
        public long IsOwner { get; set; }

        [JsonProperty("A_H")]
        public string AH { get; set; }

        [JsonProperty("temp_f")]
        public string TempF { get; set; }

        [JsonProperty("humidity")]
        public string Humidity { get; set; }

        [JsonProperty("pressure")]
        public string Pressure { get; set; }

        [JsonProperty("AGE")]
        public long Age { get; set; }

        [JsonProperty("Stats")]
        public string Stats { get; set; }
    }
}
