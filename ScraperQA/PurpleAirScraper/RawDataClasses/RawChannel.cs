﻿using Newtonsoft.Json;
using System;

namespace ScraperQA.PurpleAirScraper.RawDataClasses
{
    public partial class RawChannel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("latitude")]
        public string Latitude { get; set; }

        [JsonProperty("longitude")]
        public string Longitude { get; set; }

        [JsonProperty("field1")]
        public string Field1 { get; set; }

        [JsonProperty("field2")]
        public string Field2 { get; set; }

        [JsonProperty("field3")]
        public string Field3 { get; set; }

        [JsonProperty("field4")]
        public string Field4 { get; set; }

        [JsonProperty("field5")]
        public string Field5 { get; set; }

        [JsonProperty("field6")]
        public string Field6 { get; set; }

        [JsonProperty("field7")]
        public string Field7 { get; set; }

        [JsonProperty("field8")]
        public string Field8 { get; set; }

        [JsonProperty("created_at")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public DateTimeOffset UpdatedAt { get; set; }

        [JsonProperty("last_entry_id")]
        public long LastEntryId { get; set; }
    }
}
