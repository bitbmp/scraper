﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ScraperQA.PurpleAirScraper.RawDataClasses
{
    // To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
    //
    //    using QuickType;
    //
    //    var rawRecords = RawRecords.FromJson(jsonString);


    public partial class RawRecords
    {
        [JsonProperty("channel")]
        public RawChannel Channel { get; set; }

        [JsonProperty("feeds")]
        public List<RawFeed> Feeds { get; set; }
    }

    public partial class RawRecords
    {
        public static RawRecords FromJson(string json) => JsonConvert.DeserializeObject<RawRecords>(json, Converter.Settings);
    }

    public static partial class Serialize
    {
        public static string ToJson(this List<RawRecords> self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

}
