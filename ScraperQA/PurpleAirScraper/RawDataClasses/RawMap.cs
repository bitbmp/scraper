﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.Globalization;

namespace ScraperQA.PurpleAirScraper.RawDataClasses
{
    public partial class RawMap
    {
        [JsonProperty("mapVersion")]
        public long MapVersion { get; set; }

        [JsonProperty("baseVersion")]
        public long BaseVersion { get; set; }

        [JsonProperty("mapVersionString")]
        public string MapVersionString { get; set; }

        [JsonProperty("results")]
        public List<RawStation> Stations { get; set; }

    }

    public partial class RawMap
    {
        public static RawMap FromJson(string json) => JsonConvert.DeserializeObject<RawMap>(json, Converter.Settings);
    }

    public static partial class Serialize
    {
        public static string ToJson(this List<RawMap> self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
