﻿using Newtonsoft.Json;
using System;

namespace ScraperQA.PurpleAirScraper.RawDataClasses
{
    public partial class RawFeed
    {
        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("entry_id")]
        public long EntryId { get; set; }

        [JsonProperty("field1")]
        public double? Field1 { get; set; }

        [JsonProperty("field2")]
        public double? Field2 { get; set; }

        [JsonProperty("field3")]
        public double? Field3 { get; set; }

        [JsonProperty("field4")]
        public double? Field4 { get; set; }

        [JsonProperty("field5")]
        public double? Field5 { get; set; }

        [JsonProperty("field6")]
        public double? Field6 { get; set; }

        [JsonProperty("field7")]
        public double? Field7 { get; set; }

        [JsonProperty("field8")]
        public double? Field8 { get; set; }
    }
}
