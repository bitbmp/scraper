﻿using ScraperQA.Database;
using ScraperQA.PurpleAirScraper.RawDataClasses;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ScraperQA.PurpleAirScraper
{
    public class Converters
    {
        public static readonly Func<RawRecords, Station, IEnumerable<Record>> RawRecordsToDbRecords = (rrs, station) =>
        {
            List<Record> res = new List<Record>();
            if(station == null)
            {
                return res;
            }
            foreach (RawFeed rf in rrs.Feeds)
            {
                for(int i = 0; i < 5; i++ )
                {
                    Record rec = new Record();
                    rec.Date = rf.CreatedAt;
                    rec.Span = TimeSpan.Zero;

                    rec.Latitude = station.Latitude;
                    rec.Longitude = station.Longitude;
                    rec.Altitude = station.Altitude == null ? 0 : (double)station.Altitude;

                    double? val = null;
                    int vt = 0;
                    switch (i)
                    {
                        case 0:
                            val = rf.Field1;
                            vt = (int)Database.ValueType.PM1;
                            break;
                        case 1:
                            val = rf.Field2;
                            vt = (int)Database.ValueType.PM25;
                            break;
                        case 2:
                            val = rf.Field3;
                            vt = (int)Database.ValueType.PM10;
                            break;
                        case 3:
                            val = rf.Field6;
                            vt = (int)Database.ValueType.Temperature;
                            break;
                        case 4:
                            val = rf.Field7;
                            vt = (int)Database.ValueType.Humidity;
                            break;
                    }

                    if(val == null)
                    {
                        continue;
                    }

                    rec.Value = (double)val;
                    rec.ValueType = vt;

                    rec.AggregationType = (int)AggregationType.Instantaneous;

                    rec.Source = station.Id;
                    res.Add(rec);
                }
            }

            return res;
        };

        public static readonly Func<List<RawStation>, List<Station>> RawStationsToStations = raws =>
        {
            List<Station> res = new List<Station>();
            foreach (RawStation rs in raws)
            {
                if(rs.ParentId != null)
                {
                    continue;
                }

                foreach (Station stat in RawStationToStation.Invoke(rs))
                {
                    /*Search twin station*/
                    foreach (RawStation rs2 in raws)
                    {
                        if (rs2.ParentId != null && (int)rs2.ParentId == stat.Id)
                        {
                            stat.Twin = RawStationToStation.Invoke(rs2).FirstOrDefault();
                            break;
                        }
                    }

                    res.Add(stat);
                }
            }
            return res;
        };

        public static readonly Func<RawStation, IEnumerable<Station>> RawStationToStation = rs =>
        {
            List<Station> res = new List<Station>();
            int id = (int)rs.Id;
            string name = rs.Label;
            double alt = 0;

            if (rs.Lon == null)
            {
                return Enumerable.Empty<Station>();
            }
            double lon = (double)rs.Lon;

            if (rs.Lat == null)
            {
                return Enumerable.Empty<Station>();
            }
            double lat = (double)rs.Lat;

            bool isInside = rs.DeviceLocationtype == "inside";

            string hardware = rs.Type;

            int tsid = 0;
            if (!int.TryParse(rs.ThingspeakPrimaryId, out tsid))
            {
                return Enumerable.Empty<Station>();
            }

            if (String.IsNullOrWhiteSpace(rs.ThingspeakPrimaryIdReadKey))
            {
                return Enumerable.Empty<Station>();
            }
            string tskey = rs.ThingspeakPrimaryIdReadKey;

            Station twin = new Station();

            Station s = new Station(id, twin, name, lat, lon, alt, isInside, hardware, tsid, tskey);

            res.Add(s);

            return res;
        };

    }
}
