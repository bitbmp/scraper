﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;

namespace ScraperQA.OpenAQ
{
    public class ResultSet<T> where T : IResult
    {
        [JsonProperty("meta")]
        public Meta Meta { get; set; }

        [JsonProperty("results")]
        public List<T> Results { get; set; }

        public static ResultSet<T> GetResults(QueryParams query)
        {
            string q = null;
            Type t = typeof(T);

            if (t.IsSubclassOf(typeof(City))) q = "cities";
            else if (t.Equals(typeof(Country))) q = "countries";
            else if (t.Equals(typeof(Location))) q = "locations";
            else if (t.Equals(typeof(Measure))) q = "measurements";
            else if (t.Equals(typeof(Parameter))) q = "parameters";
            else if (t.Equals(typeof(Source))) q = "sources";
            else if (t.Equals(typeof(Latest))) q = "latest";

            string url = "https://api.openaq.org/v1/" + q + (query == null ? "": query.GetQuery());
            WebClient client = new WebClient();
            string data = client.DownloadString(url);

            return JsonConvert.DeserializeObject<ResultSet<T>>(data);
        }
    }
}
