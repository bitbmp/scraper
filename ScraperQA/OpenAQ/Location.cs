﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ScraperQA.OpenAQ
{
    public class Location : IResult
    {
        [JsonProperty("location")]
        public string Name { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("count")]
        public long Count { get; set; }

        [JsonProperty("sourceNames")]
        public List<string> SourceNames { get; set; }

        [JsonProperty("lastUpdated")]
        public DateTimeOffset LastUpdated { get; set; }

        [JsonProperty("firstUpdated")]
        public DateTimeOffset FirstUpdated { get; set; }

        [JsonProperty("parameters")]
        public List<Parameter> Parameters { get; set; }

        [JsonProperty("sourceName")]
        public string SourceName { get; set; }

        [JsonProperty("coordinates", NullValueHandling = NullValueHandling.Ignore)]
        public Coordinates Coordinates { get; set; }

        [JsonProperty("distance", NullValueHandling = NullValueHandling.Ignore)]
        public double? Distance { get; set; }
    }
}
