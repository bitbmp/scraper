﻿using Newtonsoft.Json;

namespace ScraperQA.OpenAQ
{
    public class AveragingPeriod
    {
        [JsonProperty("value")]
        public long Value { get; set; }

        [JsonProperty("unit")]
        public string Unit { get; set; }
    }
}
