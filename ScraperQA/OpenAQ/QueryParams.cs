﻿using System;
using System.Globalization;
using System.Text;

namespace ScraperQA.OpenAQ
{
    public class QueryParams
    {
        private StringBuilder query;
        public QueryParams()
        {
            query = new StringBuilder("?");
        }
        public QueryParams Clone()
        {
            return new QueryParams() { query = new StringBuilder(query.ToString()) };
        }

        public QueryParams Country(string countryName)
        {
            query.Append("country=").Append(countryName).Append("&");
            return this;
        }
        public QueryParams City(string cityName)
        {
            query.Append("city=").Append(cityName).Append("&");
            return this;
        }
        public QueryParams Location(int location)
        {
            query.Append("location=").Append(location).Append("&");
            return this;
        }
        public QueryParams Parameter(string parameter)
        {
            query.Append("parameter=").Append(parameter).Append("&");
            return this;
        }
        public QueryParams HasGeo(bool hasGeo)
        {
            query.Append("has_geo=").Append(hasGeo ? "true" : "false").Append("&");
            return this;
        }
        public QueryParams Coordinates(double lat, double lon)
        {
            query.Append("coordinates=").Append(lat.ToString(CultureInfo.InvariantCulture)).Append(',').Append(lon.ToString(CultureInfo.InvariantCulture)).Append("&");
            return this;
        }
        public QueryParams ValueFrom(double valueFrom)
        {
            query.Append("value_from=").Append(valueFrom.ToString(CultureInfo.InvariantCulture)).Append("&");
            return this;
        }
        public QueryParams ValueTo(double valueTo)
        {
            query.Append("value_to=").Append(valueTo.ToString(CultureInfo.InvariantCulture)).Append("&");
            return this;
        }
        public QueryParams DateFrom(DateTime date)
        {
            query.Append("date_from=").Append(date.ToString("yyyy-MM-ddTHH:mm:ss")).Append("&");
            return this;
        }
        public QueryParams DateTo(DateTime date)
        {
            query.Append("date_to=").Append(date.ToString("yyyy-MM-ddTHH:mm:ss")).Append("&");
            return this;
        }
        public QueryParams Radius(double meter)
        {
            query.Append("radius=").Append(meter.ToString(CultureInfo.InvariantCulture)).Append("&");
            return this;
        }
        public QueryParams OrderBy(string field, bool asc = true)
        {
            query.Append("order_by[]=").Append(field).Append("&").Append("sort[]=").Append(asc ? "asc" : "desc").Append("&");
            return this;
        }
        public QueryParams Limit(int limit)
        {
            query.Append("limit=").Append(limit).Append("&");
            return this;
        }
        public QueryParams Page(int page)
        {
            query.Append("page=").Append(page).Append("&");
            return this;
        }
        public QueryParams Page(ExtraFields field)
        {
            query.Append("include_fields[]=").Append(field).Append("&");
            return this;
        }


        public string GetQuery()
        {
            return query.ToString();
        }
        public override string ToString()
        {
            return GetQuery();
        }
    }
    public enum ExtraFields
    {
        attribution,
        averagingPeriod,
        sourceName,
    }
}
