﻿using Newtonsoft.Json;

namespace ScraperQA.OpenAQ
{
    public class Parameter : IResult
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("preferredUnit")]
        public string PreferredUnit { get; set; }
    }
}
