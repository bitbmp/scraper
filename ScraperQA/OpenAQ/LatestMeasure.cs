﻿using Newtonsoft.Json;
using System;

namespace ScraperQA.OpenAQ
{
    public class LatestMeasure
    {
        [JsonProperty("parameter")]
        public string Parameter { get; set; }

        [JsonProperty("value")]
        public double Value { get; set; }

        [JsonProperty("lastUpdated")]
        public DateTimeOffset LastUpdated { get; set; }

        [JsonProperty("unit")]
        public string Unit { get; set; }

        [JsonProperty("sourceName")]
        public string SourceName { get; set; }

        [JsonProperty("averagingPeriod", NullValueHandling = NullValueHandling.Ignore)]
        public AveragingPeriod AveragingPeriod { get; set; }
    }
}
