﻿using Newtonsoft.Json;

namespace ScraperQA.OpenAQ
{
    public class Country : IResult
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("cities")]
        public long Cities { get; set; }

        [JsonProperty("locations")]
        public long Locations { get; set; }

        [JsonProperty("count")]
        public long Count { get; set; }
    }
}
