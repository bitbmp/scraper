﻿using Newtonsoft.Json;
using System;

namespace ScraperQA.OpenAQ
{
    public class Date
    {
        [JsonProperty("utc")]
        public DateTimeOffset Utc { get; set; }

        [JsonProperty("local")]
        public DateTimeOffset Local { get; set; }
    }
}
