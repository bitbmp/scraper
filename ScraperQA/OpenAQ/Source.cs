﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ScraperQA.OpenAQ
{
    public class Source : IResult
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("adapter")]
        public string Adapter { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("region", NullValueHandling = NullValueHandling.Ignore)]
        public string Region { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("sourceURL")]
        public string SourceUrl { get; set; }

        [JsonProperty("contacts")]
        public List<string> Contacts { get; set; }

        [JsonProperty("active")]
        public bool Active { get; set; }

        [JsonProperty("city", NullValueHandling = NullValueHandling.Ignore)]
        public string City { get; set; }

        [JsonProperty("resolution", NullValueHandling = NullValueHandling.Ignore)]
        public string Resolution { get; set; }

        [JsonProperty("timezone", NullValueHandling = NullValueHandling.Ignore)]
        public string Timezone { get; set; }

        [JsonProperty("location", NullValueHandling = NullValueHandling.Ignore)]
        public string Location { get; set; }

        [JsonProperty("organization", NullValueHandling = NullValueHandling.Ignore)]
        public string Organization { get; set; }
    }
}
