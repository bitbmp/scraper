﻿using Newtonsoft.Json;
using ScraperQA.Database;
using ScraperQA.Utility;
using ScraperQA.Utility.Unit;
using System;

namespace ScraperQA.OpenAQ
{
    public class Measure : IResult
    {
        [JsonProperty("location")]
        public string Location { get; set; }

        [JsonProperty("parameter")]
        public string Parameter { get; set; }

        [JsonProperty("date")]
        public Date Date { get; set; }

        [JsonProperty("value")]
        public double Value { get; set; }

        [JsonProperty("unit")]
        public string Unit { get; set; }

        [JsonProperty("coordinates")]
        public Coordinates Coordinates { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        public Record? ToRecord()
        {
            if (Coordinates == null)
            {
                Logger.Log("Coordinates are nulls", LogType.Warning, false);
                return null;
            }
            double v = Value;
            Database.ValueType vt = 0;
            switch (Unit)
            {
                case "ppm": v = UnitConverter.ppm_ugm3(v, Parameter.ToUpper()); break;
                case "Âµg/mÂ³": break;
                default: Logger.Log("Unknow unit " + Unit, LogType.Warning, false); return null;
            }
            switch (Parameter.ToUpper())
            {
                case "BC": vt = Database.ValueType.BLACK_CARBON; break;
                case "CO": vt = Database.ValueType.CO; break;
                case "NO2": vt = Database.ValueType.NO2; break;
                case "O3": vt = Database.ValueType.O3; break;
                case "PM10": vt = Database.ValueType.PM10; break;
                case "PM25": vt = Database.ValueType.PM25; break;
                case "SO2": vt = Database.ValueType.SO2; break;
                default: Logger.Log("Unknow parameter " + Parameter, LogType.Warning, false); return null;
            }
            return new Record()
            {
                Date = this.Date.Utc.UtcDateTime,

                Latitude = this.Coordinates.Latitude,
                Longitude = this.Coordinates.Longitude,

                Altitude = 0, //??
                Source = Location.GetHashCode(), //??
                AggregationType = (int)AggregationType.Instantaneous, //??
                Span = TimeSpan.FromSeconds(0), //??
                Value = v,
                ValueType = (int)vt
            };
        }
    }
}
