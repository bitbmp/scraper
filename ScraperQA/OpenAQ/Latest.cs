﻿using Newtonsoft.Json;
using ScraperQA.Database;
using ScraperQA.Utility;
using ScraperQA.Utility.Unit;
using System;
using System.Collections.Generic;

namespace ScraperQA.OpenAQ
{
    public class Latest : IResult
    {
        [JsonProperty("location")]
        public string Location { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("measurements")]
        public List<LatestMeasure> Measurements { get; set; }

        [JsonProperty("distance", NullValueHandling = NullValueHandling.Ignore)]
        public double? Distance { get; set; }

        [JsonProperty("coordinates", NullValueHandling = NullValueHandling.Ignore)]
        public Coordinates Coordinates { get; set; }

        public List<Record> ToRecord()
        {
            if (Coordinates == null)
            {
                Logger.Log("Coordinates are nulls", LogType.Warning, false);
                return new List<Record>();
            }
            List<Record> list = new List<Record>();

            foreach (var m in Measurements)
            {
                double v = m.Value;
                Database.ValueType vt = 0;
                switch (m.Unit)
                {
                    case "ppm": v = UnitConverter.ppm_ugm3(v, m.Parameter.ToUpper()); break;
                    case "Âµg/mÂ³": break;
                    default: Logger.Log("Unknow unit " + m.Unit, LogType.Warning, false); return null;
                }
                switch (m.Parameter.ToUpper())
                {
                    case "BC": vt = Database.ValueType.BLACK_CARBON; break;
                    case "CO": vt = Database.ValueType.CO; break;
                    case "NO2": vt = Database.ValueType.NO2; break;
                    case "O3": vt = Database.ValueType.O3; break;
                    case "PM10": vt = Database.ValueType.PM10; break;
                    case "PM25": vt = Database.ValueType.PM25; break;
                    case "SO2": vt = Database.ValueType.SO2; break;
                    default: Logger.Log("Unknow parameter " + m.Parameter, LogType.Warning, false); return null;
                }
                list.Add(new Record()
                {
                    Date = m.LastUpdated.UtcDateTime,

                    Latitude = this.Coordinates.Latitude,
                    Longitude = this.Coordinates.Longitude,

                    Altitude = 0, //??
                    Source = Location.GetHashCode(), //??
                    AggregationType = m.AveragingPeriod != null ? (int)AggregationType.Average : (int)AggregationType.Instantaneous, //??
                    Span = AveragingPeriodToTimeSpan(m.AveragingPeriod),
                    Value = v,
                    ValueType = (int)vt
                });
            }
            return list;
        }
        private TimeSpan AveragingPeriodToTimeSpan(AveragingPeriod ap)
        {
            if (ap == null)
                return TimeSpan.FromSeconds(0);
            if (ap.Unit == "seconds")
                return TimeSpan.FromSeconds(ap.Value);
            if (ap.Unit == "minutes")
                return TimeSpan.FromMinutes(ap.Value);
            if (ap.Unit == "hours")
                return TimeSpan.FromHours(ap.Value);
            if (ap.Unit == "days")
                return TimeSpan.FromDays(ap.Value);
            return TimeSpan.FromSeconds(0);
        }
    }
}
