﻿using Newtonsoft.Json;

namespace ScraperQA.OpenAQ
{
    public class Meta
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("license")]
        public string License { get; set; }

        [JsonProperty("website")]
        public string Website { get; set; }

        [JsonProperty("page")]
        public long Page { get; set; }

        [JsonProperty("limit")]
        public long Limit { get; set; }

        [JsonProperty("found")]
        public long Found { get; set; }
    }
}
