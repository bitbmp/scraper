﻿using Newtonsoft.Json;

namespace ScraperQA.OpenAQ
{
    public class City : IResult
    {
        [JsonProperty("city")]
        public string Name { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("locations")]
        public long Locations { get; set; }

        [JsonProperty("count")]
        public long Count { get; set; }
    }
}
